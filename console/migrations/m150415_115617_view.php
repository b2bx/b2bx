<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_115617_view extends Migration
{
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%view}}', [
            'id' => Schema::TYPE_PK,
            'message_id' => Schema::TYPE_INTEGER. ' NULL',
            'user_id' => Schema::TYPE_INTEGER. ' NULL',
            'view' => Schema::TYPE_INTEGER.' NULL',
            'timestamp' => Schema::TYPE_INTEGER.' NULL',
            'status' => Schema::TYPE_SMALLINT . ' NULL DEFAULT 0'
            
        ], $tableOptions);
        
        $this->addForeignKey('FK_view_message', '{{%view}}', 'message_id', '{{%message}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('FK_view_user', '{{%view}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down() {
        $this->dropTable('{{%view}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
