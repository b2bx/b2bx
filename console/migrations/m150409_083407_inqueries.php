<?php

use yii\db\Schema;
use yii\db\Migration;

class m150409_083407_inqueries extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%inquery}}', [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER. ' NULL',
            'user_id' => Schema::TYPE_INTEGER. ' NULL',
            'message' => Schema::TYPE_STRING. '(512) NULL',
            'number' => Schema::TYPE_STRING . ' NULL',
            'quantity' => Schema::TYPE_INTEGER . ' NULL DEFAULT 0',            
            'price' => Schema::TYPE_DECIMAL . '(9, 3) NULL DEFAULT 0', 
            'timestamp' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_SMALLINT . ' NULL DEFAULT 0'
            
        ], $tableOptions);
        
        $this->addForeignKey('FK_inquery_product', '{{%inquery}}', 'product_id', '{{%product}}', 'id', 'SET NULL', 'RESTRICT');
        $this->addForeignKey('FK_inquery_user', '{{%inquery}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down() {
        $this->dropTable('{{%inquery}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
