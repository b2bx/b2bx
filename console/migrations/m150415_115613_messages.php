<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_115613_messages extends Migration
{
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%message}}', [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER. ' NULL',
            'from_user_id' => Schema::TYPE_INTEGER. ' NULL',
            'to_user_id' => Schema::TYPE_INTEGER. ' NULL',
            'text' => Schema::TYPE_STRING. '(1024) NULL',
            'timestamp' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_SMALLINT . ' NULL DEFAULT 0'
            
        ], $tableOptions);
        
        $this->addForeignKey('FK_message_product', '{{%message}}', 'product_id', '{{%product}}', 'id', 'SET NULL', 'RESTRICT');
        $this->addForeignKey('FK_message_user_from', '{{%message}}', 'from_user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('FK_message_user_to', '{{%message}}', 'to_user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down() {
        $this->dropTable('{{%message}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
