<?php

use yii\db\Schema;
use yii\db\Migration;

class m150430_111814_message extends Migration {

    public function safeUp() {
        $this->addColumn("{{%message}}", 'model', Schema::TYPE_STRING . ' NULL AFTER `product_id`');
        $this->addColumn("{{%message}}", 'model_id', Schema::TYPE_INTEGER . ' NULL AFTER `model`');
        $this->execute('ALTER TABLE {{%message}} ADD INDEX  `Model_class`(`model`);');
        $this->dropForeignKey('FK_message_product', "{{%message}}");
        $this->dropColumn("{{%message}}", 'product_id');
    }

    public function safeDown() {
        $this->execute('ALTER TABLE {{%message}} DROP INDEX Model_class;');
        $this->dropColumn("{{%message}}", 'model_id');
        $this->dropColumn("{{%message}}", 'model');
        $this->addColumn("{{%message}}", 'product_id', Schema::TYPE_INTEGER . ' NULL AFTER `id`');
        $this->addForeignKey('FK_message_product', '{{%message}}', 'product_id', '{{%product}}', 'id', 'SET NULL', 'RESTRICT');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
