<?php

use yii\db\Schema;
use yii\db\Migration;

class m150506_111448_wish_list extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%wish_list}}', [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER. ' NULL',
            'user_id' => Schema::TYPE_INTEGER. ' NULL',
            'timestamp' => Schema::TYPE_INTEGER.' NULL',
            'status' => Schema::TYPE_SMALLINT.' NULL',
        ], $tableOptions);
        
        $this->addForeignKey('FK_wish_product', '{{%wish_list}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('FK_wish_user', '{{%wish_list}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        //$this->execute("ALTER TABLE  {{%wish_list}} ADD UNIQUE  `wish` (  `product_id` ,  `user_id` ) COMMENT  '';");
    }

    public function down() {
        $this->dropTable('{{%wish_list}}');
    }
}
