<?php

use yii\db\Schema;
use yii\db\Migration;

class m150330_133855_store extends Migration
{
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%store}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING,
            'domain' => Schema::TYPE_STRING. ' NOT NULL',
            
        ], $tableOptions);
        
        $this->createTable('{{%product}}', [
            'id' => Schema::TYPE_PK,
            'store_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'number' => Schema::TYPE_STRING . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL', 
            'quantity' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',            
            'price' => Schema::TYPE_DECIMAL . '(9, 3) NOT NULL DEFAULT 1', 
            'visible' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1'
        ], $tableOptions);
        
        $this->createTable('{{%product_detail}}', [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'description' => Schema::TYPE_STRING . ' NOT NULL',           
        ], $tableOptions);
        
        $this->createTable('{{%product_image}}', [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'path' => Schema::TYPE_STRING . ' NOT NULL',
            'filename' => Schema::TYPE_STRING . ' NOT NULL DEFAULT 1',            
            'extention' => Schema::TYPE_STRING . '(10) NOT NULL',            
            'position' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0'
        ], $tableOptions);
        
        $this->addForeignKey('FK_product_store', '{{%product}}', 'store_id', '{{%store}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('FK_product_detail', '{{%product_detail}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('FK_product_image', '{{%product_image}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
        
        $this->insert('{{%store}}', [
            'id' => 1,
            'title' => 'Test store',
            'domain' => 'http://localhost', 
        ]);
        
        $this->insert('{{%product}}', [
            'id' => 1,
            'store_id' => 1,
            'number' => '45GH-67JE',
            'title' => 'Some product', 
            'quantity' => 999,            
            'price' => '1.5', 
            'visible' => 1
        ]);
        $this->insert('{{%product_detail}}', [
            'product_id' => 1,
            'description' => 'Some product description',
        ]);
    }

    public function safeDown() {
         $this->dropTable('{{%product_image}}');
         $this->dropTable('{{%product_detail}}');
         $this->dropTable('{{%product}}');
         $this->dropTable('{{%store}}');
    }
}
