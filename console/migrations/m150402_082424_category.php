<?php

use yii\db\Schema;
use yii\db\Migration;

class m150402_082424_category extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%category}}', [
            'id' => Schema::TYPE_PK,
            'parent_id' => Schema::TYPE_INTEGER. ' NULL',
            'title' => Schema::TYPE_STRING,
            'path' => Schema::TYPE_STRING. '(100) NULL',
            'visible' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1'
            
        ], $tableOptions);
        
        $this->addColumn('{{%product}}', 'category_id', Schema::TYPE_INTEGER.' AFTER `store_id` ');
        $this->insert('{{%category}}', [
            'id' => 1,
            'title' => 'Test category',
            'path' => 'test', 
            'visible' => 1,
        ]);
        $this->update('{{%product}}', ["category_id" => 1]);
        $this->addForeignKey('FK_product_category', '{{%product}}', 'category_id', '{{%category}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down() {
        $this->dropForeignKey('FK_product_category', '{{%product}}');
        $this->dropColumn('{{%product}}', 'category_id');
        $this->dropTable('{{%category}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
