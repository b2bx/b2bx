<?php

use yii\db\Schema;
use yii\db\Migration;

class m150428_112547_trade extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%trade}}', [
            'id' => Schema::TYPE_PK,
            'product_id' => Schema::TYPE_INTEGER . ' NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NULL',
            'action' => Schema::TYPE_INTEGER . ' NULL', //add - 1, remove - 2
            'amount' => Schema::TYPE_INTEGER . ' NULL',
            'type' => Schema::TYPE_SMALLINT . ' NULL', //unit - 1, pcs - 2
            'notice' => Schema::TYPE_STRING . '(512) NULL',
            'timestamp' => Schema::TYPE_INTEGER . ' NULL',
            'status' => Schema::TYPE_SMALLINT . ' NULL DEFAULT 0'
                ], $tableOptions);

        $this->addForeignKey('FK_trade_product', '{{%trade}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('FK_trade_user', '{{%trade}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down() {
        $this->dropTable('{{%trade}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
