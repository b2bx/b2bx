<?php

use yii\db\Schema;
use yii\db\Migration;

class m150505_094831_trade_update extends Migration {

    public function up() {
        $this->dropForeignKey('FK_trade_product', '{{%trade}}');
        $this->addForeignKey('FK_trade_product', '{{%trade}}', 'product_id', '{{%product}}', 'id', 'SET NULL', 'RESTRICT');
        $this->dropForeignKey('FK_trade_user', '{{%trade}}');
        $this->addForeignKey('FK_trade_user', '{{%trade}}', 'user_id', '{{%user}}', 'id', 'SET NULL', 'RESTRICT');
    }

    public function down() {
        $this->dropForeignKey('FK_trade_product', '{{%trade}}');
        $this->addForeignKey('FK_trade_product', '{{%trade}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->dropForeignKey('FK_trade_user', '{{%trade}}');
        $this->addForeignKey('FK_trade_user', '{{%trade}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }
}
