<?php

use yii\db\Schema;
use yii\db\Migration;

class m150429_072012_world_country extends Migration {

    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%world_country}}', [
            'id' => Schema::TYPE_PK,
            'code' => Schema::TYPE_STRING. '(2) NULL',
            'code3' => Schema::TYPE_STRING. '(3) NULL',
            'name' => Schema::TYPE_STRING.'(52) NULL',
            'continent' => "enum('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') NOT NULL DEFAULT 'Asia'",
            'region' => Schema::TYPE_STRING . '(26) NULL',
            'surface_area' => Schema::TYPE_FLOAT . '(10, 2) NULL DEFAULT 0',
            'indep_year' => Schema::TYPE_SMALLINT . ' NULL',
            'population' => Schema::TYPE_INTEGER . ' NULL DEFAULT 0',
            'life_expectancy' => Schema::TYPE_FLOAT . '(3, 1) NULL',
            'gnp' => Schema::TYPE_FLOAT . '(10, 2) NULL',
            'gnpold' => Schema::TYPE_FLOAT . '(10, 2) NULL',
            'local_name' => Schema::TYPE_STRING . '(45) NULL',
            'government_form' => Schema::TYPE_STRING . '(45) NULL',
            'head_of_state' => Schema::TYPE_STRING . '(60) NULL',
            'capital' => Schema::TYPE_INTEGER . ' NULL',
            'latitude' => Schema::TYPE_DECIMAL . '(9, 5) NULL',
            'longitude' => Schema::TYPE_DECIMAL . '(9, 5) NULL',
            'KEY `code` (`code`)',
            'KEY `code3` (`code3`)'
        ], $tableOptions);
        
        $this->createTable('{{%world_city}}', [
            'id' => Schema::TYPE_PK,
            'country_id' => Schema::TYPE_INTEGER. ' NULL',
            'country_code' => Schema::TYPE_STRING. '(3) NULL',
            'name' => Schema::TYPE_STRING.'(35) NULL',
            'district' => Schema::TYPE_STRING.'(20) NULL',
            'population' => Schema::TYPE_INTEGER . ' NULL DEFAULT 0',
            //'KEY `country_id` (`country_id`)',
            'KEY `country_code` (`country_code`)'
        ], $tableOptions);
        
        $this->addForeignKey('FK_city_country', '{{%world_city}}', 'country_id', '{{%world_country}}', 'id', 'CASCADE', 'RESTRICT');
        
        $sqlFile = dirname(__FILE__)."/data/world_cities.sql";
        $sql = file_get_contents($sqlFile);
            $this->execute($sql);
    }

    public function safeDown() {
        $this->dropTable('{{%world_city}}');
        $this->dropTable('{{%world_country}}');
    }
}
