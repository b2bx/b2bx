<?php

use yii\db\Schema;
use yii\db\Migration;

class m150429_070358_product_new_fields extends Migration {

    public function up() {
        $this->addColumn("{{%product}}", 'warehouse', Schema::TYPE_STRING . ' NULL AFTER `price`');
        $this->addColumn("{{%product}}", 'pieces', Schema::TYPE_INTEGER . ' NULL AFTER `warehouse`');

        $this->addColumn("{{%product}}", 'pieces_per_unit', Schema::TYPE_INTEGER . ' NULL AFTER `pieces`');
        $this->addColumn("{{%product}}", 'unit', Schema::TYPE_STRING . ' NULL AFTER `pieces_per_unit`');
        $this->addColumn("{{%product}}", 'price_per_unit', Schema::TYPE_DECIMAL . '(9,3) NULL AFTER `unit`');
        $this->addColumn("{{%product}}", 'currency', Schema::TYPE_STRING . '(3) NULL AFTER `price_per_unit`');
        $this->addColumn("{{%product}}", 'moq', Schema::TYPE_INTEGER . ' NULL AFTER `currency`'); //USD | EUR
        
        //Удаляем старые поля
        //$this->dropColumn("{{%product}}", 'price');
        //$this->dropColumn("{{%product}}", 'quantity');
    }

    public function down() {
        $this->dropColumn("{{%product}}", 'moq');
        $this->dropColumn("{{%product}}", 'currency');
        $this->dropColumn("{{%product}}", 'price_per_unit');
        $this->dropColumn("{{%product}}", 'unit');
        $this->dropColumn("{{%product}}", 'pieces_per_unit');
        $this->dropColumn("{{%product}}", 'pieces');
        $this->dropColumn("{{%product}}", 'warehouse');

        //Добавляем станые поля
        //$this->addColumn("{{%product}}", 'quantity', Schema::TYPE_INTEGER . ' NULL DEFAULT 0 AFTER `title`');
        //$this->addColumn("{{%product}}", 'price', Schema::TYPE_DECIMAL . '(9, 3) NULL DEFAULT 0 AFTER `quantity`');
    }
}
