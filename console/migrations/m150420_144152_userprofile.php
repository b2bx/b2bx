<?php

use yii\db\Schema;
use yii\db\Migration;

class m150420_144152_userprofile extends Migration {
 
    //ALTER TABLE `profile` ADD `email` VARCHAR( 255 ) NULL AFTER `user_id` ,
    //ADD `avatar` VARCHAR( 255 ) NULL AFTER `email` ;
    public function up() {
        $this->addColumn("{{%profile}}", 'email', Schema::TYPE_STRING.' NULL AFTER `user_id`');
        $this->addColumn("{{%profile}}", 'avatar', Schema::TYPE_STRING.' NULL AFTER `email`');
        $this->execute('ALTER TABLE {{%user}} ADD UNIQUE (`username`);');
    }

    public function down() {
        $this->dropColumn("{{%profile}}", 'avatar');
        $this->dropColumn("{{%profile}}", 'email');
        $this->execute('ALTER TABLE {{%user}} DROP INDEX username;');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
