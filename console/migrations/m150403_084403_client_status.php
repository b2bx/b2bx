<?php

use yii\db\Schema;
use yii\db\Migration;

class m150403_084403_client_status extends Migration {

    public function up() {
        $this->addColumn('{{%user}}', 'deny_to', Schema::TYPE_INTEGER.' AFTER `status` ');
        $this->addColumn('{{%user}}', 'note', Schema::TYPE_STRING.' AFTER `deny_to` ');
    }

    public function down() {
        $this->dropColumn('{{%user}}', 'note');
        $this->dropColumn('{{%user}}', 'deny_to');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
