<?php
namespace console\controllers;
 
use yii\console\Controller;
use common\models\WishList;
 
/**
 * Test controller
 */
class CronController extends Controller {
 
    public function actionIndex() {
        echo "cron service runnning";
    }
 
    //yii cron/clearwishlist 30
    public function actionClearwishlist($period = 30) {
        echo "Clear wish list witch items is {$period} days old\n";
        echo "Update items count: ".WishList::clearItems($period)."\n";
    }
 
}

