<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use common\models\Product;

/**
 * UploadForm is the model behind the upload form.
 */
class ProductFromData extends Model {

    /**
     * @var UploadedFile file attribute
     */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['file'], 'file', 'checkExtensionByMimeType' => false, 'extensions' => 'csv'/*, 'mimeTypes' => 'text/plain'*/],
        ];
    }

    public function attributeLabels() {
        return [
            'file' => Yii::t('product', "Choose csv file"),
        ];
    }

    /*
     *  @return result array("success" => [<ids list>], "errors" => [])
     */

    public function import() {
        $handle = fopen($this->file->tempName,"r"); 
        $fields = fgetcsv($handle,1000,",","'");
        $count = count($fields);
        $result = array("success" => [], "errors" => []);
        do { 
            if (isset($data[0])) { 
                $product = new \common\models\ProductForm;
                $product->store_id = \common\models\Store::homeStore()->id;
                foreach ($data as $key => $value) {
                    $f = @$fields[$key];
                    $product->$f = $value;
                }
                if ($product->validate()) {
                    if ($id = $product->saveModel()) {
                        $result['success'][] = $id;
                    }
                }
                else {$result['errors'][] = $product->getErrors (); $result['failed']++;}
            } 
        } while ($data = fgetcsv($handle,1000,",","'"));
        return $result;
    }
    
    private function buildSQL($fields, $data) {
        $sql = "INSERT INTO ".Product::tableName()." (`".  implode("`,`", $fields)."`) VALUES (";
                $need_comma = 0;
                foreach ($data as $key => $value) {
                    $sql .= ($need_comma++?", ":"")."'".addslashes($value)."'";
                }
        $sql .= ");";
        return $sql;
    }

    /*
     * get columns from selected table
     * $table - db table
     * @return array list of db columns
     *
     */

    public function tableColumns($table) {
        return Yii::app()->getDb()->getSchema()->getTable($table)->getColumnNames();
    }

}
