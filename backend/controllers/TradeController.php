<?php

namespace backend\controllers;

use Yii;
use common\models\Trade;
use common\models\Product;
use common\models\Message;

class TradeController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new Trade();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }
    
    public function actionView($id = false) {
        $model = Trade::findOne(["id" => $id]);
        $message = Message::generateBaseModel(['model' => $model]);
        
        //Если есть сообщение, то обрабатываем его и завершаем запрос
        if (Message::Send()) 
            Yii::$app->end();
        
        $searchModel = new Message();
        $dataProvider = $searchModel->searchByTrade(Yii::$app->request->get(), $model->id);
        
        return $this->render("view", ["model" => $model, "searchModel" => $searchModel, "dataProvider" => $dataProvider, "message" => $message]);
    }
    
    public function actionProcess($id) {
        Trade::updateAll(["status" => Trade::STATUS_IN_PROCESS], ["id" => $id]);
    }
    
    public function actionDelete($id) {
        $trade = Trade::findOne(["id" => $id]);
        if ($trade) {
            if ($product = Product::isValid($trade->product_id)) {
                $amount = $trade->calcAmount($product->pieces_per_unit); //Можно брать из базы продукта и вычислять;
                if ($product && Product::updatePiece($trade->product_id, $trade->amount, (-1 * $trade->action))) {
                    Trade::updateAll(["status" => Trade::STATUS_CANCELLED], ["id" => $id]);
                    return true;
                }
            }
            
            Trade::updateAll(["status" => Trade::STATUS_FAILED], ["id" => $id]);
        }
        
    }

}
