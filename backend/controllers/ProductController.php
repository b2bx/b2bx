<?php

namespace backend\controllers;

use Yii;
use \yii\web\Controller;
use \common\models\Category;
use common\models\Product;
use common\models\ProductForm;
use common\models\ProductSearch;
use app\models\ProductFromData;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use common\models\Trade;
use common\models\WorldCountry;

class ProductController extends Controller {

    public function actionIndex() {
        $trade = new Trade;
        $this->checkTrade();
        $grid = $this->gridViewOfProduct(Yii::$app->request->get());
        return $this->render('index', ["grid" => $grid, "trade" => $trade]);
    }

    private function gridViewOfProduct($params, $ids = false) {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search($params, $ids);
        return $this->renderPartial('grid', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionUpdate($id = false) {
        $model = new ProductForm;
        $model->loadModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstances($model, 'image');
            if ($model->validate()) {
                $model->updateModel($id);
                return $this->redirect("/product/index");
            } else {print_r($model->getErrors()); die();}
        }

        return $this->render('_form', ['model' => $model]);
    }

    public function actionAdd() {
        $model = new ProductForm;
        $model->loadModel();
        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstances($model, 'image');
            if ($model->validate()) {
                $model->saveModel();
                return $this->redirect("/product/index");
            } else {print_r($model->getErrors()); die();}
        }

        return $this->render('_form', ['model' => $model]);
    }

    protected function checkTrade() {
        if (Trade::checkAndCreate(Yii::$app->request->post())) 
            return $this->render('/trade/_form', ['model' => new Trade, ]);
        
    }

    public function actionDelete($id = false) {
        Product::deleteAll(["id" => $id]);
        if (Yii::$app->getRequest()->isAjax) {
            return $this->gridViewOfProduct([]);
        }
        return $this->redirect(['index']);
    }
    
    public function actionVisible($id = false) {
        Product::inverceVisibleStatus($id);
        if (Yii::$app->getRequest()->isAjax) {
            return $this->gridViewOfProduct([]);
            Yii::$app->end();
        }
        return $this->redirect(['index']);
    }

    public function actionImport() {
        $model = new ProductFromData();
        $result = false;
        $items = false;
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->file && $model->validate()) {
                $result = $model->import();
                if (!empty($result['success'])) {
                    Yii::$app->session['import'] = $result['success'];
                    $items = $this->gridViewOfProduct([], $result['success']);
                }
            }
        } else if (Yii::$app->request->isGet)
            $items = $this->gridViewOfProduct(Yii::$app->request->get(), @Yii::$app->session['import']);

        return $this->render('import', ['model' => $model, "result" => $result, "items" => $items]);
    }

    public function actionImagedelete($id = false) {
        $model = \common\models\ProductImage::findOne(["id" => $id]);
        if ($model && $model->delete())
            return json_encode(["success" => true]);
    }

    public function actionGenerate() {
        Category::deleteAll();
        Product::deleteAll();
        Product::recursiveFoldersDelete();
        //Генерируем новые данные
        $store = \common\models\Store::homeStore()->id;
        for ($c = 1; $c <= 5; $c++) {
            $category = new Category;
            $category->title = "Category title {$c}";
            $category->path = "category{$c}";
            $category->visible = 1;
            if ($category->save()) {
                for ($p = 1; $p <= 15; $p++) {
                    $product = new Product;
                    $product->store_id = $store;
                    $product->category_id = $category->id;
                    $product->generateNumber();
                    $product->title = "Product title {$c}_{$p}";
                    //$product->quantity = rand(1, 999);
                    //$product->price = rand(1, 999) / 100.0;
                    $product->warehouse = WorldCountry::getRandomCode();
                    $count_unit = rand(1, 999);
                    $piece_per_unit = rand(1, 500);
                    $product->pieces = $count_unit * $piece_per_unit;
                    $product->pieces_per_unit = $piece_per_unit;
                    $unit = ["Box", "Large", "Small"];
                    $product->unit = $unit[rand(0, 2)];
                    $product->price_per_unit = rand(1, 999);
                    $currency = ["usd", "eur"];
                    $product->currency = $currency[rand(0, 1)];
                    $product->moq = rand(1, 10);
                    
                    $product->visible = 1;
                    if ($product->save()) {
                        $detail = new \common\models\ProductDetail;
                        $detail->product_id = $product->id;
                        $detail->description = "Product description {$c}_{$p}";
                        $detail->save();
                        $product->uploadImages([1, 2]);
                    }
                }
            }
        }
        $this->redirect("index");
    }

}
