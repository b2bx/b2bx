<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'geo', 'sxgeo'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    public function actionSxgeo() {
        $city = Yii::$app->sypexGeo->getCity("94.153.174.102");
        print_r($city);
        Yii::$app->end();
    }
    
    public function actionGeo() {
        $ip = $_SERVER['REMOTE_ADDR'];
        $Info = \Yii::createObject([
            'class' => '\rmrevin\yii\geoip\HostInfo',
            'host' => '78.47.184.139', // some host or ip
        ]);
        // check available
        $test = $Info->isAvailable();
        header("Content-Type: plain/text");
        print_r($Info->getData());

        // obtaining all data
        $Info->getData();
        
        $Info->getContinentCode(); // NA
        $Info->getCountryCode();   // US
        $Info->getCountryCode3();  // USA
        $Info->getCountryName();   // United States
        $Info->getRegion();        // MI
        $Info->getRegionName();    // Michigan
        $Info->getCity();          // Southfield
        $Info->getPostalCode();    // 48075
        $Info->getLatitude();      // 42.465000152588
        $Info->getLongitude();     // -83.230697631836
        $Info->getDmaCode();       // 505
        $Info->getAreaCode();      // 248
        //$Info->getTimeZone();      // America/New_York
        Yii::$app->end();
    }

}
