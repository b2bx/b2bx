<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\Profile;
use common\models\UserProfileSearch;

class ClientController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new UserProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }
    
    public function actionDelete($id = false) {
        User::deleteAll(["id" => $id]);
        return $this->redirect(['index']);
    }
    
    public function actionLock($id = false) {
        User::setStatusDeny($id);
        return $this->redirect(['index']);
    }
    
    public function actionActive($id = false) {
        User::setStatusActive($id);
        return $this->redirect(['index']);
    }
    
    public function actionHide($id = false) {
        User::setStatusHidden($id);
        return $this->redirect(['index']);
    }
    
    public function actionAdd() {
        $userModel = new \frontend\models\SignupForm;
        $userModel->setDefaultValue();
        $profileModel = new Profile;
        $profileModel->user_id = 0;
        if ($userModel->load(Yii::$app->request->post()) && $profileModel->load(Yii::$app->request->post()) ) {
            if ($userModel->validate()) {
                if ($user = $userModel->signup()) {
                    $profileModel->user_id = $user->id;
                    if ($profileModel->_temp_date) $profileModel->birthday = strtotime($profileModel->_temp_date);
                    if ($profileModel->validate()) {
                        $profileModel->save();
                        return $this->redirect('index');
                    }
                    else print_r($profileModel); echo "1"; die();
                }
            }
        }
        return $this->render('_form', ['user' => $userModel, 'profile' => $profileModel]);
    }

    public function actionUpdate($id) {
        $userModel = User::findOne(["id" => $id]);
        if (!$userModel) return $this->redirect ("add");
        $profileModel = Profile::findOne(["user_id" => $id]);
        if (!$profileModel) {
            $profileModel = new Profile;
            $profileModel->user_id = $id;
        }
        if ($profileModel->birthday) 
            $profileModel->_temp_date = Yii::$app->formatter->asDate($profileModel->birthday, 'dd.MM.yyyy');
        if ($userModel->load(Yii::$app->request->post()) && $profileModel->load(Yii::$app->request->post()) ) {
            if ($userModel->validate()) {
                if ($userModel->update()) {
                    $profileModel->birthday = strtotime($profileModel->_temp_date);
                    if ($profileModel->validate()) {
                        $profileModel->save();
                        return $this->redirect('index');
                    }
                }
            }
        }
        return $this->render('_form', ['user' => $userModel, 'profile' => $profileModel, "update" => 1]);
    }

}
