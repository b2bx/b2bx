<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use \common\models\Category;

class CategoryController extends Controller {

    public function actionIndex() {
        $searchModel = new Category;
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionAdd() {
        $model = new Category;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                return $this->redirect ("/category/index");
            }
        }

        return $this->render('update', ['model' => $model,]);
    }
    
    public function actionUpdate($id = false) {
        $model = Category::findOne(["id" => $id]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->update ();
                return $this->redirect ("/category/index");
            }
        }

        return $this->render('update', ['model' => $model,]);
    }
    
    public function actionDelete($id = false) {
        Category::deleteAll(["id" => $id]);
        return $this->redirect(['index']);
    }

}
