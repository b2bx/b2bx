<?php

namespace backend\controllers;

use Yii;
use \yii\web\Controller;
use common\models\Inquery;
use \common\models\Product;
use common\models\User;
use common\models\Message;

class InqueryController extends \yii\web\Controller {

    public function actionIndex() {
        $searchModel = new Inquery();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }
    
    public function actionAdd() {
        $model = new Inquery;
        $model->message = "System inquery";
        if ($model->load(Yii::$app->request->post())) {
            if ($product = Product::findOne(["id" => $model->product_id]))
                $model->number = $product->number;
            if ($model->validate()) {
                $model->save ();
                return $this->redirect("index");
            }
        }

        return $this->render('_add', ['model' => $model, ]);
    }

    public function actionUpdate($id = false) {
        $model = Inquery::findOne(["id" => $id]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->update ();
                return $this->redirect("index");
            }
        }
        
        $message = Message::generateBaseModel(['model' => $model]);
        //Если есть сообщение, то обрабатываем его и завершаем запрос
        if (Message::Send()) 
            Yii::$app->end();
        
        $searchModel = new Message();
        $dataProvider = $searchModel->searchByInquery(Yii::$app->request->get(), $model->id);

        return $this->render('_update', ['model' => $model, "searchModel" => $searchModel, "dataProvider" => $dataProvider, "message" => $message]);
    }
    
    public function actionReply($id = false) {
        $model = Inquery::prepareForReply($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($product = Product::findOne(["id" => $model->product_id]))
                $model->number = $product->number;
            if ($model->validate()) {
                $model->save();
                return $this->redirect("index");
            }
        }

        return $this->render('_add', ['model' => $model, "reply" => true]);
    }
    
    public function actionView($id) {
        Inquery::updateAll(["status" => Inquery::STATUS_IN_VIEW], ["id" => $id]);
    }
    
    public function actionProcess($id) {
        Inquery::updateAll(["status" => Inquery::STATUS_IN_PROCESS], ["id" => $id]);
    }
    
    public function actionDelete($id) {
        //Inquery::updateAll(["status" => Inquery::STATUS_DELETED], ["id" => $id]);
        Inquery::deleteAll(["id" => $id]);
    }
    
    public function actionClose($id) {
        Inquery::updateAll(["status" => Inquery::STATUS_CLOSED], ["id" => $id]);
    }

}
