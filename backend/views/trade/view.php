<?php
use common\models\Trade;
use common\models\User;
use common\models\Message;
use yii\widgets\DetailView;
use common\models\Product;
use yii\helpers\Html;

$this->params['breadcrumbs'] = [ 
    ["label" => "Trades", "url" => "/trade/index"],
    "View trade"
];
?>
<h1>View trade</h1>
<?php
$product = ($model->product_id)?Product::isValid($model->product_id):false;
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        
        [
            'label' => 'Product',
            'format'=>'raw',
            'value' => !empty($product)?Html::a($product->title, "/product/update?id=".$model->product_id):false,
        ],
        [
            'label' => 'From client',
            'format' => 'raw',
            'value' => !empty($model->user_id)?Html::a(User::getUsername($model->user_id), ["client/update", "id" => $model->user_id]):"none",
        ],
        [
            'label' => 'Date',
            'value' => $model->timestamp?date("Y-m-d G:i:s", $model->timestamp):"none",
        ],
        [
            'label' => 'Action',
            'format' => 'raw',
            'value' => $model->getActionAttribute()." ".$model->amount. " pieces",
        ],
        [
            'attribute' => 'type',
            'format' => 'raw',
            'value' => $model->getTypeAttribute(),
        ],
        'notice',
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => $model->getStatusAttribute(),
        ],
    ]
]);

?>
<?= $this->render("/message/_list", ['dataProvider' => $dataProvider, "model" => Message::generateBaseModel(), "message" => $message]) ?>

