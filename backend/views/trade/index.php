<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;
use common\models\User;
use common\models\Product;
use common\models\Trade;
use common\models\Message;

$this->params['breadcrumbs'] = [ 
    "Trades", 
];
?>
<h1>Trades overview</h1>

<p>
    <?php
    $gridColumns = [
        'id' => [
            'attribute' => 'id',
            'filter' => false,
        ],
        [
            'attribute' => 'product_title',
            'filter' => false,
            'format' => 'raw',
            'value' => function($model) {
                if ($model->product)
                    return Html::a($model->product->title, ["product/update", "id" => $model->product_id]);
            }
        ],
        'user_id' => [
            'attribute' => 'username',
            'filter' => false,
            'format' => 'raw',
            'value' => function($model) {
                return Html::a(User::getUsername($model->user_id), ["client/update", "id" => $model->user_id]);
            }
        ],
        [
            'attribute' => 'action',
            'format' => 'raw',
            'filter' => false,
            'value' => function($model) {
                return $model->getActionAttribute();
            }
        ],
        [
            'attribute' => 'amount',
            'filter' => false,
        ],
        [
            'attribute' => 'type',
            'filter' => false,
            'format' => 'raw',
            'value' => function($model) {
                return $model->getTypeAttribute();
            }
        ],
        [
            'attribute' => 'notice',
            'filter' => false,
        ],
        'timestamp' => [
            'attribute' => 'timestamp',
            'format' => 'raw',
            'filter' => false,
            'value' => function ($model) {                      
                return ($model->timestamp)?date("Y-m-d G:i:s", $model->timestamp):"";
            },
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'filter' => false,
            'value' => function($model) {
                return $model->getStatusAttribute();
            },
        ],
        [
            'label' => 'Messages',
            'format' => 'raw',
            'value' => function($model) {
                return Message::countComments($model);
            },
        ],
        [  
        'class' => 'yii\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:110px;'],
        'header'=>'Actions',
        'buttons'=>[
//            'update' => function ($url, $model) {
//                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
//                    'title' => Yii::t('yii', 'Update'),
//                    'data-pjax'=>'0',
//                ]);
//            },
            'view' => function ($url, $model) {
                if (!Yii::$app->user->can("view")) return;
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                    'title' => Yii::t('yii', 'View'),
                ]);
            },
            'process' => function ($url, $model) {
                if (!Yii::$app->user->can("status")) return;
                return Html::a('<span class="glyphicon glyphicon-share-alt"></span>', $url, [
                    'title' => Yii::t('yii', 'Process'),
                    'data-pjax'=>'0',
                    'class' => 'grid-action'
                ]);
            },
            'delete' => function ($url, $model) {
                if (!Yii::$app->user->can("delete")) return;
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'title' => Yii::t('yii', 'Delete'),
                    'data-pjax' => '0', 
                    'class' => 'grid-action'
                ]);
            }
        ],
        'template' => '{view} {process} {delete}',

       ],

    ];
    Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]);
    Pjax::end();
    ?>
</p>
<!--<p>
    <?= Html::a(Yii::t('client', 'Add new trade'), ['add'], ['class' => 'btn btn-success']) ?>
</p>-->

