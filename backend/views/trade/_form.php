<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Trade;

/* @var $this yii\web\View */
/* @var $model common\models\Trade */
/* @var $form ActiveForm */
?>
<div class="product-trade">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'product_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'action')->dropDownList($model->getRangeList('action')) ?>
        <?= $form->field($model, 'amount') ?>
        <?= $form->field($model, 'type')->dropDownList($model->getRangeList('type')) ?>
        <?= $form->field($model, 'notice')->textarea() ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div><!-- product-trade -->
