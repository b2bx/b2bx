<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use common\models\User;
use dosamigos\fileupload\FileUploadUI;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form ActiveForm */

$this->params['breadcrumbs'] = [ 
    ["label" => "Clients", "url" => "/client/index"],
    isset($update)?$user->username:"New user"
];
?>
<h1><?= empty($update)?"Create new ":"Update " ?>user</h1>
<div class="client-update">
    
    <?php  
    $form = ActiveForm::begin(/*[
                //'enableClientValidation' => false,
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
    ]*/);
    ?>
    
        
    
        <?= $form->field($profile, 'user_id')->hiddenInput()->label('') ?>
        <?php 
        if (!isset($update)) {
            echo $form->field($user, 'username');
            echo $form->field($user, 'password');
        }   
        ?>
        <?= $form->field($user, 'email') ?>
             
        <?= $form->field($profile, 'firstname') ?>
        <?= $form->field($profile, 'lastname') ?>
        <?php $form->field($user, 'note')->textarea() ?>
        <?php 
        echo $form->field($profile, '_temp_date')->widget(DatePicker::className(), [
            'dateFormat' => 'yyyy-MM-dd',
            'options' => [
                'autoclose' => true,
            ]
        ]);
        ?>
        <?php 
        $_user = new User;
        echo $form->field($user, 'status')->dropDownList($_user->getStatusList());
        ?>
    
        <div class="form-group">
            <?= Html::submitButton(isset($update)?Yii::t('client', 'Update'):Yii::t('client', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- client-update -->
