<?php
/* @var $this yii\web\View */
use \Yii;
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;
use common\models\User;

$this->params['breadcrumbs'] = [ 
    "Clients", 
];
?>
<h1><?= Yii::t("client", "Clients overview") ?></h1>
<p>
<script type="text/javascript">
    $(function(){
        $('body').on('click', '.grid-action', function(e){
            var href = $(this).attr('href');
            var self = this;
            $.get(href, function(){
                var pjax_id = $(self).closest('.pjax-wraper').attr('id');
                $.pjax.reload('#' + pjax_id);
            });
            return false;
        })
    });
</script>
<?php
$gridColumns = [
    'id' => [
        'attribute' => 'id',
        'filter' => false,
    ], 
    'username',
    'email',
    [
        'format' => 'raw',
        'header' => 'FIO',
        'value' => function($model) {
            return $model->profile['firstname']." ".$model->profile['lastname'];
        },
        'filter' => false,
    ],
    
//    [
//        'attribute' => 'firstname',
//        'value' => 'profile.firstname'
//    ],
//    [
//        'attribute' => 'lastname',
//        'value' => 'profile.lastname'
//    ],
    [
        'attribute' => 'note',
        'filter' => false,
    ],
    
    [
        'attribute' => 'birthday',
        'format' => 'raw',
        'filter' => false,
        'value' => function ($model) {                      
            return ($model->profile['birthday'])?date("Y-m-d", $model->profile['birthday']):"";
        },
    ],
    [
        'attribute' => 'status',
        'format' => 'raw',
        'filter' => false,
        'value' => function($model) {
            return $model->getStatusAttribute();
        },
    ],
    [  
        'class' => 'yii\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:110px;'],
        'header'=>'Actions',
        'buttons'=>[
            'update' => function ($url, $model) {
                if (!Yii::$app->user->can("/client/update")) return;
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'title' => Yii::t('yii', 'Update'),
                    'data-pjax'=>'w0',
                ]);
            },
            'active' => function ($url, $model) {
                if (!Yii::$app->user->can("/client/active")) return;
                return Html::a('<span class="glyphicon glyphicon-check"></span>', $url, [
                    'title' => Yii::t('yii', 'Active'),
                    'data-pjax'=>'w0',
                ]);
            },
            'lock' => function ($url, $model) {
                if (!Yii::$app->user->can("/client/lock")) return;
                return Html::a('<span class="glyphicon glyphicon-lock"></span>', $url, [
                    'title' => Yii::t('yii', 'Deny'),
                    'data-pjax'=>'w0',
                ]);
            },
            'hide' => function ($url, $model) {
                if (!Yii::$app->user->can("/client/hide")) return;
                return Html::a('<span class="glyphicon glyphicon-eye-close"></span>', $url, [
                    'title' => Yii::t('yii', 'Hide'),
                    'data-pjax'=>'w0',
                ]);
            },
            'delete' => function ($url, $model) {
                if (!Yii::$app->user->can("/client/delete")) return;
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'title' => Yii::t('yii', 'Delete'),
                    'data-pjax' => '0', 
                    'class' => 'grid-action'
                ]);
            }
        ],
        'visible' => (Yii::$app->user->can("client") || Yii::$app->user->can("client_status")),
        'template' => '{update} {active} {lock} {hide} {delete}',

       ],
        
];
Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
]);
Pjax::end();
?>
</p>
<p>
    <?php if (Yii::$app->user->can("/client/add")) { ?>
    <?= Html::a(Yii::t('client', 'Add new client'), ['add'], ['class' => 'btn btn-success']) ?>
    <?php } ?>
</p>

