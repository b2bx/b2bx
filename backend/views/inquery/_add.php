<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\Product;
use common\models\User;
use common\models\Inquery;
use yii\helpers\ArrayHelper;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Inquery */
/* @var $form ActiveForm */
$this->params['breadcrumbs'] = [ 
    ["label" => "Inqueries", "url" => "/inquery/index"],
    isset($reply)?"Reply inquery":"New inquery", 
];
?>
<h1><?= isset($reply)?"Reply":"New" ?> inquery</h1>

<div class="form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, '_skip')->hiddenInput()->label(false) ?>
        <?php
        echo $form->field($model, 'product_id')->hiddenInput()->label(false);
        $product = Product::find()->select(['title as label', 'id'])->asArray()->all();
        echo $form->field($model, 'product')->widget(AutoComplete::classname(), [
            'name' => 'product',
            'clientOptions' => [
                'source' => $product,
                'autoFill'=>true,
                'minLength'=> 0,
                'select' => new JsExpression("function( event, ui ) {
                    $('#inquery-product_id').val(ui.item.id);
                }")
            ],
            'options' => ['class' => 'combobox'] //Автооткрытие по клику
        ]);
        ?>
        <?php
        echo $form->field($model, 'user_id')->hiddenInput()->label(false);
        $user = User::find()->select(['username as label', 'id'])->asArray()->all();
        echo $form->field($model, 'username')->widget(AutoComplete::classname(), [
            'name' => 'username',
            'clientOptions' => [
                'source' => $user,
                'autoFill'=>true,
                'minLength'=> 0,
                'select' => new JsExpression("function( event, ui ) {
                    $('#inquery-user_id').val(ui.item.id);
                }")
            ],
            'options' => ['class' => 'combobox'] //Автооткрытие по клику
        ]);
        ?>
        <?php 
        echo $form->field($model, '_temp_date')->widget(DatePicker::className(), [
            'dateFormat' => 'yyyy-MM-dd',
            'options' => [
                'autoclose' => true,
            ]
        ]);
        ?>
        <?= $form->field($model, 'price') ?>
        <?= $form->field($model, 'quantity') ?>
        <?= $form->field($model, 'message')->textarea() ?>
        <?php 
        $_inquery = new Inquery;
        echo $form->field($model, 'status')->dropDownList($_inquery->getRangeList('status'));
        ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('inquery', isset($reply)?"Reply":"Update"), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- _form -->
