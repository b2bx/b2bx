<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\Product;
use common\models\User;
use common\models\Inquery;
use common\models\Message;

/* @var $this yii\web\View */
/* @var $model common\models\Inquery */
/* @var $form ActiveForm */
$this->params['breadcrumbs'] = [ 
    ["label" => "Inqueries", "url" => "/inquery/index"],
    "View inquery", 
];
?>
<h1>View inquery</h1>
<?php 
$product = ($model->product_id)?Product::isValid($model->product_id):false;
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        
        [
            'label' => 'Product',
            'format'=>'raw',
            'value' => !empty($product)?Html::a($product->title." (".$model->number.")", "/product/update?id=".$model->product_id):$model->number,
        ],
        [
            'label' => 'From client',
            'format' => 'raw',
            'value' => !empty($model->user_id)?Html::a(User::getUsername($model->user_id), ["client/update", "id" => $model->user_id]):"none",
        ],
        [
            'label' => 'Date',
            'value' => $model->timestamp?date("Y-m-d G:i:s", $model->timestamp):"none",
        ],
        'message'
    ]
]);
?>

<div class="form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'price') ?>
        <?= $form->field($model, 'quantity') ?>
        <?php 
        $_inquery = new Inquery;
        echo $form->field($model, 'status')->dropDownList($_inquery->getRangeList('status'));
        ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('inquery', 'Update'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- _form -->

<h1>Messages</h1>
<?= $this->render("/message/_list", ['dataProvider' => $dataProvider, "model" => Message::generateBaseModel(), "message" => $message]) ?>
