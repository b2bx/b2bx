<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;
use common\models\User;

$this->params['breadcrumbs'] = [ 
    "Inqueries", 
];
?>
<h1>Inqueries overview</h1>

<p>
    <?php
    $gridColumns = [
        'id' => [
            'attribute' => 'id',
            'filter' => false,
        ],
        'user_id' => [
            'attribute' => 'user_id',
            'filter' => false,
            'format' => 'raw',
            'value' => function($model) {
                return Html::a(User::getUsername($model->user_id), ["client/update", "id" => $model->user_id]);
            }
        ],
        [
            'attribute' => 'number',
            'format' => 'raw',
            'value' => function($model) {
                return !empty($model->product_id)?Html::a($model->number, ["product/update", "id" => $model->product_id]):$model->number;
            }
        ],
        [
            'attribute' => 'message',
            'filter' => false,
        ],
        'quantity' => [
            'attribute' => 'quantity',
            'filter' => false,
        ],
        'price' => [
            'attribute' => 'price',
            'filter' => false,
        ],
        'timestamp' => [
            'attribute' => 'timestamp',
            'format' => 'raw',
            'filter' => false,
            'value' => function ($model) {                      
                return ($model->timestamp)?date("Y-m-d G:i:s", $model->timestamp):"";
            },
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'filter' => false,
            'value' => function($model) {
                return $model->getStatusAttribute();
            },
        ],
        [  
        'class' => 'yii\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:110px;'],
        'header'=>'Actions',
        'buttons'=>[
            'update' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'title' => Yii::t('yii', 'Update'),
                    'data-pjax'=>'0',
                ]);
            },
            'view' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                    'title' => Yii::t('yii', 'View'),
                    'data-pjax'=>'0',
                    'class' => 'grid-action'
                ]);
            },
            'process' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-share-alt"></span>', $url, [
                    'title' => Yii::t('yii', 'Process'),
                    'data-pjax'=>'0',
                    'class' => 'grid-action'
                ]);
            },
            'reply' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-repeat"></span>', $url, [
                    'title' => Yii::t('yii', 'Reply'),
                    'data-pjax'=>'0',
                    //'class' => 'grid-action'
                ]);
            },
            'close' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                    'title' => Yii::t('yii', 'Close'),
                    'data-pjax' => '0', 
                    'class' => 'grid-action'
                ]);
            },
            'delete' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'title' => Yii::t('yii', 'Delete'),
                    'data-pjax' => '0', 
                    'class' => 'grid-action'
                ]);
            }
        ],
        'template' => '{update} {reply} {close}',

       ],

    ];
    Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]);
    Pjax::end();
    ?>
</p>
<p>
    <?php if (Yii::$app->user->can("/inquery/add")) { ?>
    <?= Html::a(Yii::t('client', 'Add new inquery'), ['add'], ['class' => 'btn btn-success']) ?>
    <?php } ?>
</p>
