<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;
use common\models\WorldCountry;

?>
<p>
<?php
$gridColumns = [
    'id' => [
        'attribute' => 'id',
        'filter' => false,
    ], 
//    [
//        'attribute' => 'category',
//        'format' => 'raw',
//        'value' => function($model) {
//            return ($model->category)?Html::a($model->category->title, ["category/update", "id" => $model->category_id]):false;
//        }
//    ],
    'number' => [
        'attribute' => 'number',
        'format' => 'raw',
        'value' => function($model) {
            return $model->number;//Html::tag("span", $model->number, ["title" => Html::img("http://www.win-with-women.com/wp-content/uploads/2013/07/asdasd.jpg")]);
        }
    ],
    'title',
//    [
//        'attribute' => 'description',
//        'value' => 'detail.description'
//    ],
            
//    'quantity' => [
//        //'header' => 'Amount',
//        'attribute' => 'quantity',
//        'filter' => false,
//    ], 
//    'price' => [
//        'attribute' => 'price',
//        'filter' => false,
//    ], 
    // warehouse pieces pieces_per_unit unit price_per_unit currency moq
    [
        'attribute' => 'warehouse',
        'filter' => false,
        'format' => 'raw',
        'value' => function($model) {
            return WorldCountry::getNameByCode3($model->warehouse);
        }
    ],
    'pieces' => [
        'label' => 'Pcs',
        'attribute' => 'pieces',
        'filter' => false,
    ],
    'pieces_per_unit' => [
        'label' => 'Pcs/Unit',
        'attribute' => 'pieces_per_unit',
        'filter' => false,
    ],
    'unit',
    'price_per_unit' => [
        'label' => 'Price/Unit',
        'attribute' => 'price_per_unit',
        'filter' => false,
    ],
    'currency',
    'moq',
    [
        'attribute' => 'visible',
        'format' => 'raw',
        'filter' => false,
        'value' => function ($model) {                      
            return ($model->visible)?"<span class='text-success'>".Yii::t("main", "Yes")."</span>":"<span class='text-danger'>".Yii::t("main", "No")."</span>";
        },
    ],
    
     [  
        'class' => 'yii\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:90px;'],
        'header'=>'Actions',
        'buttons'=>[
            'update' => function ($url, $model) {
                if (!Yii::$app->user->can("update")) return;
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'title' => Yii::t('yii', 'Update'),
                    'data-pjax'=>'w0',
                ]);
            },
            'trade' => function ($url, $model) {
                if (!Yii::$app->user->can("/product/trade")) return;
                return Html::a('<span class="glyphicon glyphicon-shopping-cart"></span>', $url, [
                    'title' => Yii::t('yii', 'Trade'),
                    'data-pid' => $model->id,
                    'data-toggle' => "modal",
                    'data-target' => "#tradeForm",
                    'data-pjax'=>'w0',
                ]);
            },
            'visible' => function ($url, $model) {
                if (!Yii::$app->user->can("status")) return;
                return Html::a('<span class="glyphicon glyphicon-eye'.($model->visible?"-close":"-open").'"></span>', $url, [
                    'title' => $model->visible?Yii::t('yii', 'Hide'):Yii::t('yii', 'Show'),
                    'data-pjax'=>'0',
                    'class' => 'grid-action'
                ]);
            },
            'delete' => function ($url, $model) {
                if (!Yii::$app->user->can("delete")) return;
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'title' => Yii::t('yii', 'Delete'),
                    'data-pjax' => '0', 
                    'class' => 'grid-action'
                ]);
            }
        ],
        'template' => '{update} {trade} {visible} {delete}',

       ],
        
];
Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
]);
Pjax::end();
?>
</p>
