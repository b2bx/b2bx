<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

$this->params['breadcrumbs'] = [ 
    "Products", 
];
?>
<h1><?= Yii::t('product', 'Product overview') ?></h1>

<p><?= $grid ?></p>
<p>
    <?php if (Yii::$app->user->can("/product/add")) { ?>
    <?= Html::a(Yii::t('product', 'Add new product'), ['add'], ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('product', 'Import from csv'), ['import'], ['class' => 'btn btn-primary']) ?>
    <?php } ?>
</p>
<p>
<?php
//Отправка личного сообщения
$this->registerJs(
    "$(function() {
        $('#tradeForm').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var id = button.data('pid');
            var modal = $(this);
            modal.find('#trade-product_id').val(id);
        });
        
        $('#_add_trade').on('pjax:end', function() {
            $('#tradeForm').modal('hide');
        });
    });");
Modal::begin([
    'id' => 'tradeForm',
    'header' => '<h3 id="message-header">Add / Remove trade</h3>',
]);

Pjax::begin(['id' => '_add_trade']);
echo $this->render("/trade/_form", ["model" => $trade]);
Pjax::end();
Modal::end();
?>
</p>
