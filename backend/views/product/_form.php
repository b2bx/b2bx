<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Store;
use common\models\Category;
use kartik\file\FileInput;
use common\models\WorldCountry;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\ProductForm */
/* @var $form ActiveForm */

$models = Store::find()->All();
$storeListData = ArrayHelper::map($models, 'id', 'title');

$models = Category::find()->All();
$categoryListData = ArrayHelper::map($models, 'id', 'title');
$this->params['breadcrumbs'] = [ 
    ["label" => "Products", "url" => "/product/index"],
    !$model->isNewRecord?$model->title:"New product"
];
?>
<h1><?= $model->isNewRecord?"Create new ":"Update " ?>product</h1>
<div class="product-add">


    <?php
    $form = ActiveForm::begin([
                //'enableClientValidation' => false,
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
    ]);
    ?>
    
    
    
    <?php echo $this->render("_images", ["images" => $model->imageList]); ?>

    <?php
    echo $form->field($model, 'image[]', [
        'template' => /* "<span class=\"btn btn-default btn-file\"> Browse {input} </span>" */
        '<div class="input-group file-input">
            <span class="input-group-btn">
                <span class="btn btn-primary btn-file">Add images {input} </span>
            </span>
            <input type="text" class="form-control" readonly="">
        </div>',
    ])->fileInput(['multiple' => true])
    ?>

    <?= $form->field($model, 'store_id', array('template' => "{input}"))->hiddenInput(); ?>   

    <?= $form->field($model, 'category_id')->dropDownList($categoryListData, ['prompt' => 'Without category']) ?>
    <?= $form->field($model, 'number') ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'description') ?>
    <?php $form->field($model, 'quantity'); ?>
    <?php $form->field($model, 'price'); ?>
    <?php
    echo $form->field($model, 'warehouse_code')->hiddenInput()->label(false);
    $product = WorldCountry::find()->select(['name as label', 'code3 as `id`'])->orderBy("`name` ASC")->asArray()->all();
    echo $form->field($model, 'warehouse')->widget(AutoComplete::classname(), [
        'name' => 'warehouse',
        'clientOptions' => [
            'source' => $product,
            'autoFill'=>true,
            'minLength'=> 0,
            'select' => new JsExpression("function( event, ui ) {
                $('#productform-warehouse_code').val(ui.item.id);
            }")
        ],
        'options' => ['class' => 'combobox'] //Автооткрытие по клику
    ]);
    ?>
    <?= $form->field($model, 'pieces')->textInput(["readonly" => true]); ?>
    <?= $form->field($model, 'pieces_per_unit')->textInput(["readonly" => !$model->isNewRecord]); ?>
    <?= $form->field($model, 'unit')->label("Unit".((!empty($model->pieces_per_unit) && !$model->isNewRecord)?" (".($model->pieces / $model->pieces_per_unit).")":"")); ?>
    <?= $form->field($model, 'price_per_unit'); ?>
    <?= $form->field($model, 'currency')->dropDownList(["usd" => "USD","eur" => "EUR"]); ?>
    <?= $form->field($model, 'moq'); ?>
    
    <?= $form->field($model, 'visible')->dropDownList(["0" => Yii::t("main", "No"), "1" => Yii::t("main", "Yes")]) ?>

    <div class="form-group">
<?= Html::submitButton(($model->isNewRecord)?Yii::t('app', 'Submit'):Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>

</div><!-- product-add -->
