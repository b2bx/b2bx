<?php if (!empty($images)) { ?>
<script type="text/javascript">
    $(document).on('click', '.file-preview-frame div.close', function() {
        var id = $(this).attr("rel");
        $.get("/product/imagedelete", {id:id}, function(data) {
           if (data.success) 
               $(".file-preview-field #preview-" + id).parent().remove();
        }, "json");
    });
</script>
<!--<div class="file-preview">
    <div class="file-preview-thumbnails">
        <?php foreach ($images as $id => $image) { ?>
        <div class="file-preview-frame file-preview-initial" id="preview-<?= $id ?>">
            <img src="<?= $image['small'] ?>" class="file-preview-image">
            <div class="close" rel="<?= $id ?>">x</div>
        </div>
        <?php } ?>
    </div>
    <div class="clearfix"></div>
</div>-->
<div class="file-preview-field">
    <?php 
    $items = [];
    foreach ($images as $id => $image) { 
        $items[]['content'] = '
        <div class="file-preview-frame file-preview-initial" id="preview-'.$id.'">
            <img src="'.$image['small'].'" class="file-preview-image">
            <div class="close" rel="'.$id.'">x</div>
        </div>';
    }
    echo kartik\sortable\Sortable::widget([
        'type'=>'grid',
        'items' => $items,
        'itemOptions'=>['class'=>'file-preview-thumbnails'],
    ]);
    ?>
    <div class="clearfix"></div>
</div>
<?php } ?>
