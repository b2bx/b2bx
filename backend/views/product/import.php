<?php
$this->params['breadcrumbs'] = [ 
    ["label" => "Products", "url" => "/product/index"],
    "Imrort products from csv-file"
];
?>

<h1>Import panel</h1>

Сsv file should contain the first line of the transfer fields (<a href="/test.csv">example</a>):
<ul>
    <li><b>number</b> - String, product ID (XXXX-XXXX or e.t.)</li>
    <li><b>title</b> - String (title of product)</li>
    <li><b>quantity</b> - Integer (product quantity)</li>
    <li><b>price</b> - Decimal (float type format)</li>
    <li><b>description</b> - String (product description)</li>
    <li><b>visible</b> - 0/1 (hide/show product in frontend application)</li>
</ul>

<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
echo $form->field($model, 'file')->fileInput();
echo Html::submitButton(Yii::t('product', 'Import'), ['class' =>'btn btn-success']);
?>

<?php ActiveForm::end() ?>

<?php
if ($result) {
    echo "<h2>".Yii::t("product", "Import result:")."</h2>";
    echo "Success: ".count($result['success']).", Errors: ".count($result['errors']);
    echo $items;
    if (!empty($result['errors'])) {
        print_r($result['errors']);
    }
}
?>

