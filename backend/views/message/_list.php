<?php
use \yii\widgets\ListView;
use yii\bootstrap\Modal;
use \yii\web\JsExpression;
use yii\widgets\Pjax;
?>

<p>
<?php
//Список сообщений
Pjax::begin(['id' => '_list_message']);
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '/message/_item',
    'viewParams' => [
        "is_out" => @$is_out,
        "show_info" => @$show_info
    ]
]);
Pjax::end();
?>
</p>

<?php

//Блок, для отправки комментария к продукту
if (isset($message)) {
    $this->registerJs(
        '$("document").ready(function(){ 
             $("#_send_message1").on("pjax:end", function() {
                 $.pjax.reload({container:"#_list_message"});
             });
         });'
    );
    echo "<div class=\"form\">";
    Pjax::begin(['id' => '_send_message1']);
    echo $this->render("/message/_form", ["model" => $message, "label" => false]);
    Pjax::end();
    echo "</div>";
}
 
?>

<?php
//Отправка личного сообщения
Modal::begin([
    'id' => 'messageForm',
    'header' => '<h3 id="message-header">Send message to</h3>',
    'clientEvents' => [
        'show.bs.modal' => new JsExpression("function(event){
            var button = $(event.relatedTarget);
            var id = button.data('userid');
            var username = button.data('username');
            var modal = $(this);
            modal.find('#message-header').text('Send message to ' + username);
            modal.find('#message-to_user_id').val(id);
        }"),
    ]
]);


$this->registerJs(
   '$("document").ready(function(){ 
        $("#_send_message2").on("pjax:end", function() {
            $("#messageForm").modal("hide");
        });
    });'
);
echo "<div class=\"form\">";
Pjax::begin(['id' => '_send_message2']);
echo $this->render("_form", ["model" => $model, "label" => true]);
Pjax::end();
echo "</div>";
Modal::end();

?>

