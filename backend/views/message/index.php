<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;
use \yii\widgets\ListView;
use yii\bootstrap\Modal;
use yii\web\JsExpression;

$this->params['breadcrumbs'] = [ 
    "Messages"
];
?>
<h1>Overview <?= $title ?> messages</h1>

<?= $this->render("/message/_list", ['dataProvider' => $dataProvider, "model" => $model, "is_out" => $is_out, "show_info" => true]) ?>