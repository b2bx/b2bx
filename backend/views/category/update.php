<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form ActiveForm */

$models = Category::find()->All();
$listData=  ArrayHelper::map($models,'id','title');

$this->params['breadcrumbs'] = [ 
    ["label" => "Categories", "url" => "/category/index"],
    !$model->isNewRecord?$model->title:"New category"
];
?>
<div class="category-update">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'parent_id')->dropDownList($listData, ['prompt' => 'Without parent category']) ?>
        
        <?= $form->field($model, 'path') ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'visible')->dropDownList(["0" => Yii::t("main", "No"), "1" => Yii::t("main", "Yes")]) ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('category', ($model->isNewRecord)?'Submit':'Update'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- category-update -->
