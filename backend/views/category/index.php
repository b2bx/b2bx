<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;

$this->params['breadcrumbs'] = [ 
    "Categories", 
];
?>
<h1><?= Yii::t("category", "Category overview") ?></h1>

<p>
<?php
$gridColumns = [
    'id' => [
        'attribute' => 'id',
        'filter' => false,
    ], 
    [
        'attribute' => 'parent',
        'value' => 'parent_title'
    ],
    'title',
    
    'path' => [
        'attribute' => 'path',
        //'filter' => false,
    ], 
    [
        'attribute' => 'visible',
        'format' => 'raw',
        'filter' => false,
        'value' => function ($model) {                      
            return ($model->visible)?"<span class='text-success'>".Yii::t("main", "Yes")."</span>":"<span class='text-danger'>".Yii::t("main", "No")."</span>";
        },
    ],
    
     [  
        'class' => 'yii\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:60px;'],
        'header'=>'Actions',
        'buttons'=>[
            'update' => function ($url, $model) {
                if (!Yii::$app->user->can("update")) return;
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                    'title' => Yii::t('yii', 'Update'),
                    'data-pjax'=>'w0',
                ]);
            },
            'delete' => function ($url, $model) {
                if (!Yii::$app->user->can("delete")) return;
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'title' => Yii::t('yii', 'Delete'),
                    'data-pjax' => '0', 
                    'class' => 'grid-action'
                ]);
            }
        ],
        'template' => '{update} {delete}',

       ],
        
];
Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
]);
Pjax::end();
?>
</p>
<p>
    <?php if (Yii::$app->user->can("/category/add")) { ?>
    <?= Html::a(Yii::t('category', 'Add new category'), ['add'], ['class' => 'btn btn-success']) ?>
    <?php } ?>
</p>
