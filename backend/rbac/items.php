<?php
return [
    'All' => [
        'type' => 1,
        'description' => 'Role with all permissions',
        'ruleName' => 'AdminRule',
        'children' => [
            'index',
            'admin',
            'modify',
            'Guest',
            'Developer',
            'client',
            'client_status',
        ],
    ],
    'index' => [
        'type' => 2,
        'description' => 'Items overview',
        'children' => [
            '/trade/index',
            '/product/index',
            '/inquery/index',
            '/client/index',
            '/category/index',
        ],
    ],
    'admin' => [
        'type' => 2,
        'description' => 'Grand Accessclien',
        'children' => [
            '/admin/*',
        ],
    ],
    '/inquery/index' => [
        'type' => 2,
        'description' => 'Inquery overview',
    ],
    '/client/index' => [
        'type' => 2,
        'description' => 'Client overview',
    ],
    '/admin/*' => [
        'type' => 2,
        'description' => 'Grand access routes',
    ],
    '/trade/index' => [
        'type' => 2,
    ],
    '/product/index' => [
        'type' => 2,
        'description' => 'Product overview',
    ],
    '/category/index' => [
        'type' => 2,
        'description' => 'Category overview',
    ],
    '/inquery/*' => [
        'type' => 2,
        'description' => 'Manipulate with inqueries',
    ],
    '/product/*' => [
        'type' => 2,
        'description' => 'Manipulate with products',
    ],
    '/category/*' => [
        'type' => 2,
        'description' => 'Manipulate with categories',
    ],
    '/client/*' => [
        'type' => 2,
        'description' => 'Manipulate with clients',
    ],
    '/trade/*' => [
        'type' => 2,
        'description' => 'Manipulate with trades',
    ],
    '/inquery/update' => [
        'type' => 2,
        'description' => 'Update inqueries',
    ],
    '/product/update' => [
        'type' => 2,
        'description' => 'Update products',
    ],
    '/category/update' => [
        'type' => 2,
        'description' => 'Update categories',
    ],
    '/client/update' => [
        'type' => 2,
        'description' => 'Update clients',
    ],
    '/trade/update' => [
        'type' => 2,
        'description' => 'Update trades',
    ],
    'update' => [
        'type' => 2,
        'description' => 'Update action',
        'children' => [
            '/inquery/update',
            '/product/update',
            '/category/update',
            '/trade/update',
        ],
    ],
    '/inquery/add' => [
        'type' => 2,
        'description' => 'Add inqueries',
    ],
    '/product/add' => [
        'type' => 2,
        'description' => 'Add products',
    ],
    '/category/add' => [
        'type' => 2,
        'description' => 'Add categories',
    ],
    '/client/add' => [
        'type' => 2,
        'description' => 'Add clients',
    ],
    '/trade/add' => [
        'type' => 2,
        'description' => 'Add trades',
    ],
    'add' => [
        'type' => 2,
        'description' => 'Add action',
        'children' => [
            '/inquery/add',
            '/product/add',
            '/category/add',
            '/trade/add',
        ],
    ],
    '/inquery/view' => [
        'type' => 2,
        'description' => 'View inqueries',
    ],
    '/product/view' => [
        'type' => 2,
        'description' => 'View products',
    ],
    '/category/view' => [
        'type' => 2,
        'description' => 'View categories',
    ],
    '/client/view' => [
        'type' => 2,
        'description' => 'View clients',
    ],
    '/trade/view' => [
        'type' => 2,
        'description' => 'View trades',
    ],
    'view' => [
        'type' => 2,
        'description' => 'View action',
        'children' => [
            '/inquery/view',
            '/product/view',
            '/category/view',
            '/client/view',
            '/trade/view',
        ],
    ],
    '/inquery/delete' => [
        'type' => 2,
        'description' => 'Delete inqueries',
    ],
    '/product/delete' => [
        'type' => 2,
        'description' => 'Delete products',
    ],
    '/category/delete' => [
        'type' => 2,
        'description' => 'Delete categories',
    ],
    '/client/delete' => [
        'type' => 2,
        'description' => 'Delete clients',
    ],
    '/trade/delete' => [
        'type' => 2,
        'description' => 'Delete trades',
    ],
    'delete' => [
        'type' => 2,
        'description' => 'Delete action',
        'children' => [
            '/inquery/delete',
            '/product/delete',
            '/category/delete',
            '/trade/delete',
        ],
    ],
    'modify' => [
        'type' => 2,
        'description' => 'Full actions with items (update/add/delete/close/reply/process and e.t.)',
        'children' => [
            'index',
            'update',
            'add',
            'view',
            'delete',
            'status',
            '/trade/process',
            'import',
        ],
    ],
    'Guest' => [
        'type' => 1,
        'description' => 'Actions to guest users',
        'children' => [
            'guest',
        ],
    ],
    'guest' => [
        'type' => 2,
        'description' => 'Guest user permissions',
        'children' => [
            '/site/logout',
            '/site/login',
            '/site/index',
        ],
    ],
    '/site/login' => [
        'type' => 2,
        'description' => 'Login',
    ],
    '/site/logout' => [
        'type' => 2,
        'description' => 'Logout user',
    ],
    '/debug/*' => [
        'type' => 2,
        'description' => 'Debug information',
    ],
    'Developer' => [
        'type' => 2,
        'description' => 'Developer information',
        'children' => [
            '/debug/*',
            '/product/generate',
        ],
    ],
    '/site/index' => [
        'type' => 2,
        'description' => 'Home page',
    ],
    'Manager' => [
        'type' => 1,
        'description' => 'Manager of progect',
        'children' => [
            'Guest',
            'index',
            'update',
            'view',
            'Inquery status',
            'Trade action (add/sub pieces)',
        ],
    ],
    '/inquery/reply' => [
        'type' => 2,
        'description' => 'Reply any inquery',
    ],
    '/inquery/close' => [
        'type' => 2,
        'description' => 'Close inquery',
    ],
    '/trade/process' => [
        'type' => 2,
        'description' => 'Manipulation with trade (add/sub piecies)',
    ],
    'Inquery status' => [
        'type' => 2,
        'description' => 'Change inquery status',
        'children' => [
            '/inquery/reply',
            '/inquery/close',
        ],
    ],
    'Trade action (add/sub pieces)' => [
        'type' => 2,
        'children' => [
            '/trade/index',
            '/trade/update',
            '/trade/add',
            '/trade/view',
            '/trade/delete',
            '/trade/process',
            '/product/trade',
        ],
    ],
    'General manager' => [
        'type' => 1,
        'description' => 'Top manager of progect',
        'children' => [
            'Manager',
            'delete',
            'Guest',
        ],
    ],
    'status' => [
        'type' => 2,
        'description' => 'Change status',
        'children' => [
            '/inquery/reply',
            '/inquery/close',
            '/product/visible',
        ],
    ],
    '/product/trade' => [
        'type' => 2,
        'description' => 'Add/sub pieces',
    ],
    'visible' => [
        'type' => 2,
    ],
    '/client/hide' => [
        'type' => 2,
    ],
    '/client/show' => [
        'type' => 2,
    ],
    '/product/visible' => [
        'type' => 2,
    ],
    '/product/generate' => [
        'type' => 2,
        'description' => 'For test',
    ],
    'import' => [
        'type' => 2,
        'description' => 'Import items',
        'children' => [
            '/product/import',
        ],
    ],
    '/product/import' => [
        'type' => 2,
        'description' => 'Import product from csv',
    ],
    'client_status' => [
        'type' => 2,
        'description' => 'Update user status',
        'children' => [
            '/client/hide',
            '/client/show',
            '/client/lock',
            '/client/active',
        ],
    ],
    '/client/lock' => [
        'type' => 2,
    ],
    '/client/active' => [
        'type' => 2,
    ],
    'client' => [
        'type' => 2,
        'description' => 'Add or remove user',
        'children' => [
            '/client/update',
            '/client/add',
            '/client/delete',
        ],
    ],
];
