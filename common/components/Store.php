<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\Application;
use \yii\helpers\ArrayHelper;
use common\models\Store;

class Store extends Component {

    public $project;
    private $_project;
    public $applyThemes = true;

    public static function getAvailableStores() {
        return ArrayHelper::map(Store::find()->all(), 'id', 'domain');
    }
    
    public function getCurrentStoreDomain() {
        return $this->getCurrentStore()->domain;
    }

    public function getCurrentStoreName() {
        return $this->getCurrentStore()->title;
    }


    public function getCurrentStore() {


        if ($this->_store) {
            return $this->_store;
        }

        $query = Store::find();
        $host = $_SERVER['HTTP_HOST'];

//        if (strpos($host, 'admin.socialmart.biz') !== false) {
//
//            $this->_store = Store::findOne($this->store);
//        } else {
        $query->orWhere(['like', 'domain', $host]);
//        $query->orWhere(['maindomain' => $host]);
        $this->_store = $query->one();
//        }

        if ($this->_store == null) {
            $this->_store = Store::homeStore();
        }


        return $this->_store;
    }
}