<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\Application;
use \yii\helpers\ArrayHelper;

class Image extends Component {
    
    public $imagePath = 'images';
    public $alias = 'product';
    public $dir = 'temp';
    public $filename = "";
    public $ext = "jpg";
    public $raw_data = false;
    public $default = "default_product.jpg";
    public $width = 800;
    public $height = 600;
    
    protected $thumb = null;
    protected $from_custom_file = false;


    public function set($config = array()) {
        $this->filename = substr(md5(uniqid(rand(), true)), 0, rand(7, 13)).'.'.$this->ext;
        foreach ($config as $key => $value) {
            if (isset($this->$key))
                $this->$key =  $value;
        }            
        return $this;
    }
    
    public function open($path = false, $filename = false) {
        if ($filename) {
            $this->filename = $filename;
            $path = $path."/".$filename;
        }
        if (file_exists($path)) {
            $this->thumb = new \PHPThumb\GD($path);
            $this->from_custom_file = true;
        }
        else $this->thumb = new \PHPThumb\GD($this->getDefaultImagePath());
        return $this;
    }
    
    public function resize($width, $height) {
        $this->thumb->adaptiveResizeQuadrant($width, $height);
        return $this;
    }
    
    public function save($width = false, $height = false, $one_image = false, $ext = "jpg") {
        $this->checkPath();
        $result = true;
        if ($one_image) {
            $this->width = $width;
            $this->height = $height;
        }
        $this->thumb->adaptiveResizeQuadrant ($this->width, $this->height);
        if (!$this->thumb->save($this->getImageFullPath(), $this->ext))
            $result = false;
        if ($width && $height && $result && !$one_image) {
            $this->thumb->adaptiveResizeQuadrant ($width, $height);
            if (!$this->thumb->save($this->getImageThumbFullPath(), $this->ext))
                $result = false;
        }
        
        return $result;
    }
    
    protected function checkPath() {
        $path = $this->getBaseImagePath() . $this->alias."/".$this->dir;
        if (!file_exists($path))
                @mkdir ($path, 0777, true);
    }
    
    public function getDefaultImagePath() {
        return $this->getBaseImagePath().$this->default;
    }
    
    public function getPathToImage() {
        return $this->getBaseImagePath() . $this->alias."/".$this->dir;
    }
 
    public function getImageFullPath(){
        return $this->getPathToImage()."/".$this->filename;    
    }
 
    public function getImageThumbFullPath(){
        return $this->getPathToImage()."/small_".$this->filename;    
    }
 
    public function getBaseImagePath(){
        return Yii::getAlias("@common"). '/' . $this->imagePath . '/';
    } 
    
    public function getDefaultImage($width = false, $height = false) {
        $this->raw_data = true;
        if ($width && $height) 
            return $this->open(false)->resize($width, $height)->getImage();
        else return $this->open(false)->getImage();
    }
    
    public function getDefaultAvatar($width = false, $height = false) {
        $this->raw_data = true;
        $this->default = "default_avatar.jpg";
        if ($width && $height) 
            return $this->open(false)->resize($width, $height)->getImage();
        else return $this->open(false)->getImage();
    }
    
    public function getImage($small = false) {
        $filename = (!$small)?$this->getImageFullPath():$this->getImageThumbFullPath();
        if (!$this->thumb && file_exists($filename) && !$this->from_custom_file)
            $this->thumb = new \PHPThumb\GD($filename);
        if ($this->thumb) {
            if ($this->raw_data) {
                ob_start();
                $this->thumb->show(true);
                $raw = ob_get_contents();
                ob_end_clean();
                return "data:image/{$this->ext};base64,".base64_encode($raw);
            } else $this->thumb->show ();
        }
        else return false;
    }
}

