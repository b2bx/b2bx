<?php

namespace common\models;

use Yii;
use \yii\data\ActiveDataProvider;
use common\models\Message;
use common\models\ActiveRecord;

/**
 * This is the model class for table "inquery".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $user_id
 * @property string $message
 * @property string $number
 * @property integer $quantity
 * @property string $price
 * @property integer $timestamp
 * @property integer $status
 *
 * @property Product $product
 * @property User $user
 */
class Inquery extends ActiveRecord {

    const STATUS_NEW = 1;
    const STATUS_IN_VIEW = 2;
    const STATUS_IN_PROCESS = 3;
    const STATUS_CLOSED = 4;
    const STATUS_DELETED = 5;
    
    public $_skip;
    public $username;
    public $product;
    public $_temp_date;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'inquery';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_id', 'user_id', 'quantity', 'timestamp'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_IN_VIEW, self::STATUS_IN_PROCESS, self::STATUS_CLOSED, self::STATUS_DELETED]],
            [['price'], 'number'],
            [['message'], 'string', 'max' => 512],
            [['number'], 'string', 'max' => 255],
            [['product', 'username', '_temp_date'], 'safe'],
            [['_temp_date'], 'date', 'format' => 'yyyy-MM-dd'],
            
            ['product', 'check_product', 'message' => "Product not found", "skipOnEmpty" => false],
            ['username', 'check_user', 'message' => "Client not found", "skipOnEmpty" => false],
        ];
    }
    
    public function check_product($attribute, $params) {
        if (!$this->$attribute && $this->isNewRecord && !$this->_skip) {
            $this->addError($attribute, $params['message']);
        }
    }
    
    public function check_user($attribute, $params) {
        if (!$this->$attribute && $this->isNewRecord  && !$this->_skip) {
            $this->addError($attribute, $params['message']);
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('inquery', 'ID'),
            'product_id' => Yii::t('inquery', 'Product ID'),
            'user_id' => Yii::t('inquery', 'User ID'),
            'message' => Yii::t('inquery', 'Message'),
            'number' => Yii::t('inquery', 'Number'),
            'quantity' => Yii::t('inquery', 'Quantity'),
            'price' => Yii::t('inquery', 'Price'),
            'timestamp' => Yii::t('inquery', 'Timestamp'),
            'status' => Yii::t('inquery', 'Status'),
            'username' => Yii::t('inquery', 'Select client'),
            'product' => Yii::t('inquery', 'Select product'),
            '_temp_date' => Yii::t('inquery', 'Query date'),
        ];
    }

    public static function generateBaseModel($number = false) {
        $product = Product::findProduct($number);
        $model = new Inquery;
        if (!Yii::$app->user->isGuest)
            $model->user_id = Yii::$app->user->id;
        $model->_skip = false;
        $model->timestamp = time();
        if ($product) {
            $model->product_id = $product->id;
            $model->number = $number;
            //$model->quantity = $product->quantity;
            //$model->price = $product->price;
        }
        return $model;
    }

    public function search($params, $id = false, $_active = false) {
        $query = ($id) ? Inquery::find(["user_id" => $id]) : Inquery::find();
        if ($_active)
            $query->where("status > :status", [":status" => self::STATUS_NEW]);
        $query->orderBy("timestamp DESC");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'number', $this->number]);

        return $dataProvider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if ($this->isNewRecord) {
            $message = Message::generateBaseModel([
                "model" => $this, 
                "model_id" => $this->id, //Нужно обязательно
                "from" => $this->user_id, 
                "text" => $this->message
            ]);
            if ($message->validate())
                $message->save();
            else {print_r($message->getErrors()); die();}
        }
    }
    
    public static function inWish($product_id = false, $user_id = false) {
        if (!$user_id && !Yii::$app->user->isGuest) 
            $user_id = Yii::$app->user->id;
        return Inquery::find()->where(["product_id" => $product_id, "user_id" => $user_id])->count()?true:false;
    }
    
    public function getStatusAttribute($status_state = false) {
        $status = "none";
        $s = ($status_state)?$status_state:$this->status;
        switch($s) {
            case Inquery::STATUS_NEW: 
                $status = Yii::t("client", "New");
                break;
            case Inquery::STATUS_IN_VIEW: 
                $status = Yii::t("client", "View");
                break;
            case Inquery::STATUS_IN_PROCESS: 
                $status = Yii::t("client", "Process");
                break;
            case Inquery::STATUS_CLOSED: 
                $status = Yii::t("client", "Close");
                break;
            case Inquery::STATUS_DELETED: 
                $status = Yii::t("client", "Delete");
                break;
        }
        return $status;
    }
    
    public function prepareForReply($id) {
        $model = Inquery::findOne(["id" => $id]);
        $clone = new Inquery;
        $clone->attributes = $model->attributes;
        $clone->product = Product::getTitle($model->number);
        $clone->username = User::getUsername($model->user_id);
        $clone->timestamp = time();
        $clone->_temp_date = date("Y-m-d", $clone->timestamp);
        $clone->message = "Re: ".$model->message;
        $clone->status = self::STATUS_NEW;
        return $clone;
    }

}
