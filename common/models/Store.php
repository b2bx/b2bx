<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "store".
 *
 * @property integer $id
 * @property string $title
 * @property string $domain
 *
 * @property Product[] $products
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain'], 'required'],
            [['title', 'domain'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'domain' => 'Domain',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['store_id' => 'id']);
    }
    
    /**
     * @return default Store object
     */
    public static function homeStore() {
        $store = Store::find()->one();
        if (!$store) {
            $store = new Store;
            $store->id = 1;
            $store->title = "Main store";
            $store->domain = $_SERVER['HTTP_HOST'];
        }
        return $store;
    }
}
