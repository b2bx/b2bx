<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "world_country".
 *
 * @property integer $id
 * @property string $code
 * @property string $code3
 * @property string $name
 * @property string $continent
 * @property string $region
 * @property double $surface_area
 * @property integer $indep_year
 * @property integer $population
 * @property double $life_expectancy
 * @property double $gnp
 * @property double $gnpold
 * @property string $local_name
 * @property string $government_form
 * @property string $head_of_state
 * @property integer $capital
 * @property string $latitude
 * @property string $longitude
 *
 * @property WorldCity[] $worldCities
 */
class WorldCountry extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'world_country';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['continent'], 'string'],
            [['surface_area', 'life_expectancy', 'gnp', 'gnpold', 'latitude', 'longitude'], 'number'],
            [['indep_year', 'population', 'capital'], 'integer'],
            [['code'], 'string', 'max' => 2],
            [['code3'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 52],
            [['region'], 'string', 'max' => 26],
            [['local_name', 'government_form'], 'string', 'max' => 45],
            [['head_of_state'], 'string', 'max' => 60]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'code3' => 'Code3',
            'name' => 'Name',
            'continent' => 'Continent',
            'region' => 'Region',
            'surface_area' => 'Surface Area',
            'indep_year' => 'Indep Year',
            'population' => 'Population',
            'life_expectancy' => 'Life Expectancy',
            'gnp' => 'Gnp',
            'gnpold' => 'Gnpold',
            'local_name' => 'Local Name',
            'government_form' => 'Government Form',
            'head_of_state' => 'Head Of State',
            'capital' => 'Capital',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorldCities() {
        return $this->hasMany(WorldCity::className(), ['country_id' => 'id']);
    }
    
    public static function getNameByCode($code = false) {
        return ($country = WorldCountry::findOne(["code" => $code]))?$country->name:false;
    }
    
    public static function getNameByCode3($code3 = false) {
        return ($country = WorldCountry::findOne(["code3" => $code3]))?$country->name:false;
    }
    
    public static function getRandomCode() {
        return ($country = WorldCountry::find()->orderBy("RAND()")->one())?$country->code3:false;
    }

}
