<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_image".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $path
 * @property string $filename
 * @property string $extention
 * @property integer $position
 *
 * @property Product $product
 */
class ProductImage extends \yii\db\ActiveRecord {
    
    public $image;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_image';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_id', 'path', 'extention'], 'required'],
            [['product_id', 'position'], 'integer'],
            [['path', 'filename'], 'string', 'max' => 255],
            [['extention'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'path' => 'Path',
            'filename' => 'Filename',
            'extention' => 'Extention',
            'position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    
    public function afterDelete() {
        $path = $this->path.'/'.$this->filename;
        if (file_exists($path))
            unlink ($path);
        parent::afterDelete();
    }
    
    public function getImageRawDada($option = ["small" => true]) {
        $filename = (!empty($option['small'])?"small_":"").$this->filename;
        $photo = Yii::$app->image->set(["raw_data" => true])->open($this->path, $filename);
        if (isset($option['width']) && isset($option['height']))
            $photo->resize($option['width'], $option['height']);
        return $photo->getImage();
    }

}
