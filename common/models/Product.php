<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $store_id
 * @property integer $category_id
 * @property string $number
 * @property string $title
 * @property integer $quantity
 * @property string $price
 * @property string $warehouse
 * @property integer $pieces
 * @property integer $pieces_per_unit
 * @property string $unit
 * @property number $price_per_unit
 * @property string $currency
 * @property integer $moq
 * @property integer $visible
 * @property Category $category
 * @property Store $store
 * @property ProductDetail[] $productDetails
 * @property ProductImage[] $productImages
 */
class Product extends ActiveRecord {
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['store_id', 'number', 'title'], 'required'],
            [['warehouse', 'unit', 'currency'], 'string'],
            [['store_id', 'category_id', 'quantity', 'pieces', 'pieces_per_unit', 'moq', 'visible'], 'integer'],
            [['price', 'price_per_unit'], 'number'],
            ['pieces', 'default', 'value' => 0],
            [['pieces_per_unit', 'moq'], 'default', 'value' => 1],
            [['number', 'title'], 'string', 'max' => 255]
        ];
    }
    //warehouse pieces pieces_per_unit unit price_per_unit currency moq

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('product', 'ID'),
            'store_id' => Yii::t('product', 'Store ID'),
            'category_id' => Yii::t('product', 'Category ID'),
            'number' => Yii::t('product', 'Number'),
            'title' => Yii::t('product', 'Title'),
            'quantity' => Yii::t('product', 'Total quantity'),
            'price' => Yii::t('product', 'Total price'),          
            'warehouse' => Yii::t('product', 'Warehouse'),
            'pieces' => Yii::t('product', 'Pieces'),
            'pieces_per_unit' => Yii::t('product', 'Pieces per unit'),
            'unit' => Yii::t('product', 'Unit'),
            'price_per_unit' => Yii::t('product', 'Price per unit'),
            'currency' => Yii::t('product', 'Currency'),
            'moq' => Yii::t('product', 'MOQ'),
            'visible' => Yii::t('product', 'Visible'),
        ];
    }
    
    public static function recursiveFoldersDelete($_path = false){
        $parent = Yii::getAlias('@common').'/images/product';
        if (!$_path) $_path = $parent;
        if(is_file($_path)){
            return unlink($_path);
        }
        elseif(is_dir($_path)){
            $scan = glob(rtrim($_path,'/').'/*');
            foreach($scan as $index=>$path){
                self::recursiveFoldersDelete($path);
            }
            if (strcmp($parent, $_path))
                return rmdir($_path);
            else return true;
        }
        
    }
    
    public function uploadImages($images = [], $ext = "jpg") {
        if (!empty($images)) {
            foreach ($images as $image) {
                $filename = substr(md5(uniqid(rand(), true)), 0, rand(7, 13)).'.'.$ext;
                $temp = Yii::$app->image->set(["alias" => "product", "dir" => $this->number, "ext" => $ext]);

                $i = new ProductImage();
                $i->product_id = $this->id;
                $i->path = $temp->getPathToImage();
                $i->filename = $temp->filename;
                $i->extention = $ext;
                if ($i->validate()) {
                    try {
                        $thumb = $temp->open(isset($image->tempName)?$image->tempName:false);
                        if ($thumb->save(300, 200)) {
                            $i->save();
                        }
                    }
                    catch (Exception $e) {echo $e->getMessage(); die();}
                }
            }
        }
    }
    
    public function generateNumber() {
        $md5 = md5(uniqid(rand(), true));
        $this->number = substr($md5, 0, 4)."-".substr($md5, rand(5, 10), 4);
    }
    
    public static function findProduct($number = false) {
        return Product::findOne(["number" => $number]);
    }
    
    public static function getTitle($number = false) {
        if ($product = self::findProduct($number))
            return $product->title;
        else return "unknown";
    }
    
    public static function isValid($id = false) {
        return ($product = Product::findOne(["id" => $id]))?$product:false;
    }
    
    public static function updatePiece($product_id = false, $value = 0, $action = 0) {
        if ($value < 0) return false; //Защита от хитрого инкримента
        $piece = $action * $value;
        $product = Product::findOne(["id" => $product_id]);
        if ($product) {
            $product->pieces = $product->pieces + $piece;
            $product->update();
            return true;
        }
        return false;
    }
    
    public static function inverceVisibleStatus($id) {
        $product = Product::findOne(['id' => $id]);
        if ($product) {
            if ($product->visible == 1)
                $product->visible = 0;
            else $product->visible = 1;
            $product->update();
            return $product;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore() {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetail() {
        return $this->hasOne(ProductDetail::className(), ['product_id' => 'id']);
    }
    
    public function getImage() {
        return $this->hasOne(ProductImage::className(), ['product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductDetails() {
        return $this->hasMany(ProductDetail::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages() {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }
    
    public function search($params, $category_id = false) {
        $query = Product::find();
        $query->joinWith(["image", "detail"]);
        if ($category_id)
            $query->andWhere(["category_id" => $category_id]);
        $query->groupBy("{{%product}}.`id`");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 6],
        ]);
        //echo $query->createCommand()->getRawSql();

        // load the seach form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        return $dataProvider;
    }
    
    public static function generateBaseModel() {
        
    }

}
