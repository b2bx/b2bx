<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserProfileSearch extends User {

    public $firstname;
    public $lastname;
    public $birthday;
    public $avatar;

    public function rules() {
        // only fields in rules() are searchable
        return [
            ['username', 'unique'],
            [['username', 'email', 'firstname', 'lastname', 'birthday'], 'safe'],
            ['avatar', 'file', 'extensions' => 'jpeg, jpg, gif, png', 'maxFiles' => 5],  
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'birthday' => 'Birthday',
            'avatar' => 'Avatar'
        ]);
    }
    
    public static function getUser($id = false) {
        return User::find()->with('profile')->where(["id" => $id])->one();
    }

    public function search($params, $id = false) {       
        $query = User::find();
        $query->joinWith(['profile']);
        if ($id)
            $query->andWhere (["`user`.`id`" => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        // enable sorting for the related column
        $dataProvider->sort->attributes['firstname'] = [
            'asc' => ['profile.firstname' => SORT_ASC],
            'desc' => ['profile.firstname' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['lastname'] = [
            'asc' => ['profile.lastname' => SORT_ASC],
            'desc' => ['profile.lastname' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['birthday'] = [
            'asc' => ['profile.birthday' => SORT_ASC],
            'desc' => ['profile.birthday' => SORT_DESC],
        ];

        // load the seach form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        // adjust the query by adding the filters
        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email]);
            //->andFilterWhere(['created_at' => $this->created_at])
            //->andFilterWhere(['updated_at' => $this->updated_at])
            //->andFilterWhere(['status' => $this->status]);

        $query->andFilterWhere(['like', 'profile.firstname', $this->firstname]);
        $query->andFilterWhere(['like', 'profile.lastname', $this->lastname]);
        $query->andFilterWhere(['like', 'profile.birthday', $this->birthday]);

        return $dataProvider;
    }

}
        