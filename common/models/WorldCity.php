<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "world_city".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $country_code
 * @property string $name
 * @property string $district
 * @property integer $population
 *
 * @property WorldCountry $country
 */
class WorldCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'world_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'population'], 'integer'],
            [['country_code'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 35],
            [['district'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'country_code' => 'Country Code',
            'name' => 'Name',
            'district' => 'District',
            'population' => 'Population',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(WorldCountry::className(), ['id' => 'country_id']);
    }
}
