<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "view".
 *
 * @property integer $id
 * @property integer $message_id
 * @property integer $user_id
 * @property integer $view
 * @property integer $timestamp
 * @property integer $status
 *
 * @property Message $message
 * @property User $user
 */
class View extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'view';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['message_id', 'user_id', 'view', 'timestamp', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'message_id' => 'Message ID',
            'user_id' => 'User ID',
            'view' => 'View',
            'timestamp' => 'Timestamp',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage() {
        return $this->hasOne(Message::className(), ['id' => 'message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public static function generateBaseModel($user_id = false, $message_id = false) {
        $model = new View;
        $model->message_id = $message_id;
        $model->user_id = $user_id;
        $model->view = time();
        $model->timestamp = time();
        $model->status = 1;
        return $model;
    }
    
    public static function getLastView($user_id = false) {
        return View::find(["user_id" => $user_id])->with("message")->orderBy('`timestamp` DESC')->one();
    }
    
    public static function getLastTimestamp($user_id = false) {
        $view = View::find(["user_id" => $user_id])->select('timestamp')->orderBy('`timestamp` DESC')->one();
        return (!empty($view->timestamp))?$view->timestamp:0;
    }

}
