<?php

namespace common\models;

use Yii;
use common\models\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "trade".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $user_id
 * @property integer $action
 * @property integer $amount
 * @property integer $type
 * @property string $notice
 * @property integer $timestamp
 * @property integer $status
 *
 * @property User $user
 * @property Product $product
 */
class Trade extends ActiveRecord {
    
    const TYPE_UNIT = 1;
    const TYPE_PIECE = 2;
    
    const ACTION_ADD = 1;
    const ACTION_REMOVE = -1;
    
    const STATUS_CANCELLED = 0;
    const STATUS_NEW = 1;
    const STATUS_IN_PROCESS = 2;
    const STATUS_FAILED = 9;
    const STATUS_DONE = 10;
    
    public $product_title;
    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'trade';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_id', 'user_id', 'action', 'amount', 'type', 'timestamp', 'status'], 'integer'],
            ['amount', 'required'],
            [['notice'], 'string', 'max' => 512],
            ['type', 'in', 'range' => [self::TYPE_UNIT, self::TYPE_PIECE]],
            ['action', 'in', 'range' => [self::ACTION_ADD, self::ACTION_REMOVE]],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_IN_PROCESS, self::STATUS_DONE, self::STATUS_FAILED, self::STATUS_CANCELLED]],
            [['product_title', 'username'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'user_id' => 'User ID',
            'action' => 'Action',
            'amount' => 'Amount',
            'type' => 'Type',
            'notice' => 'Notice',
            'timestamp' => 'Timestamp',
            'status' => 'Status',
            'product_title' => 'Product',
            'username' => 'User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    public function getTypeAttribute($state = false) {
        $t = ($state)?$state:$this->type;
        switch($t) {
            case self::TYPE_UNIT: 
                $type = Yii::t("trade", "Unit");
                break;
            case self::TYPE_PIECE: 
                $type = Yii::t("trade", "Piece");
                break;
            default: $type = "none";
        }
        return $type;
    }
    
    public function getActionAttribute($state = false) {
        $a = ($state)?$state:$this->action;
        switch($a) {
            case self::ACTION_ADD: 
                $action = Yii::t("trade", "Add");
                break;
            case self::ACTION_REMOVE: 
                $action = Yii::t("trade", "Remove");
                break;
            default: $action = "none";
        }
        return $action;
    }
    
    public function getStatusAttribute($state = false) {
        $a = ($state)?$state:$this->status;
        switch($a) {
            case self::STATUS_NEW: 
                $action = Yii::t("trade", "New");
                break;
            case self::STATUS_IN_PROCESS: 
                $action = Yii::t("trade", "In process");
                break;
            case self::STATUS_FAILED: 
                $action = Yii::t("trade", "Failed");
                break;
            case self::STATUS_DONE: 
                $action = Yii::t("trade", "Done");
                break;
            case self::STATUS_CANCELLED: 
                $action = Yii::t("trade", "Cancelled");
                break;
            default: $action = "none";
        }
        return $action;
    }
    
    public static function generateBaseModel() {
        $model = new Trade;
        $model->user_id = Yii::$app->user->id;
        $model->timestamp = time();
        $model->status = self::STATUS_NEW;
        return $model;
    }
    
    public function calcAmount($piece_per_unit = false) {
        return ($this->type == Trade::TYPE_UNIT && !empty($piece_per_unit))?($this->amount * $piece_per_unit):$this->amount;
    }
    
    public static function checkAndCreate($params) {
        $model = Trade::generateBaseModel();
        if ($model->load($params)) {
            if ($model->validate()) {
                if ($model->save()) {
                    if ($product = Product::isValid($model->product_id)) {
                        Product::updatePiece($model->product_id, $model->calcAmount($product->pieces_per_unit), $model->action);
                        return true;
                    }
                }
            }
        }
    }
    
    public function search($params) {
        $query = Trade::find();
        $query->select('`product`.`title`, `user`.`username`, `trade`.*');
        $query->joinWith(["product", "user"]);
        $query->orderBy("timestamp DESC");
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        // enable sorting for the related column
        $dataProvider->sort->attributes['product_title'] = [
            'asc' => ['product.title' => SORT_ASC],
            'desc' => ['product.title' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['username'] = [
            'asc' => ['user.username' => SORT_ASC],
            'desc' => ['user.username' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        

        //$query->andFilterWhere(['like', 'number', $this->number]);
        $query->andFilterWhere(['like', 'product.title', $this->product_title]);
        $query->andFilterWhere(['like', 'user.username', $this->username]);

        return $dataProvider;
    }
    

}
