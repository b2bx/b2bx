<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $path
 * @property integer $visible
 *
 * @property Product[] $products
 */
class Category extends \yii\db\ActiveRecord {
    
    public $parent;
    public $parent_title;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['parent_id', 'visible'], 'integer'],
            //[['path'], 'required'],
            [['parent'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 255],
            [['path'], 'string', 'max' => 100],
            [['parent_title'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('category', 'ID'),
            'parent_id' => Yii::t('category', 'Parent category'),
            'parent' => Yii::t('category', 'Parent category'),
            'title' => Yii::t('category', 'Title'),
            'path' => Yii::t('category', 'Path'),
            'visible' => Yii::t('category', 'Visible'),
        ];
    }
    
    protected static function addSubMenu(&$items = [], $category = false) {
        foreach ($items as $key => $item) {
            if ($key == $category['parent_id']) {
                $items[$key]['items'][$category['id']] = $category;
                return true;
            } elseif (!empty($item['items'])) {
                if (self::addSubMenu($item['items'], $category)) {
                    $items[$item['id']]['items'] = $item['items'];
                    return true;
                }  
            }
        }
        return false;
    }
    
    public static function buildMenu($active_id = false) {
        $items = [];
        $model = new Category;
        $categories = $model->search([])->getModels();
        foreach ($categories as $category) {
            if (!$category->visible) continue;
            $item = [
                "id" => $category->id,
                "parent_id" => $category->parent_id,
                "parent_title" => $category->parent_title,
                "label" => $category->title,
                "url" => "/category/".$category->path
            ];
            if ($category->id == $active_id)
                $item['linkOptions'] = ["class" => "active"];
            if ($item['parent_id']) {
                self::addSubMenu($items, $item);
            }
            else $items[$category->id] = $item;
        }
        return $items;
    }

    public function search($params) {
        $query = Category::find();
        $query->select(["`category`.*", "parent_title" => "`parent`.`title`"]);
        $query->joinWith(['parent' => function($query) {
                $query->from(['parent' => 'category']);
            }]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['parent'] = [
            'asc' => ['parent.title' => SORT_ASC],
            'desc' => ['parent.title' => SORT_DESC],
        ];

        // load the seach form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['category.parent_id' => $this->parent_id])
                ->andFilterWhere(['like', 'category.title', $this->title])
                ->andFilterWhere(['like', 'category.path', $this->path]);
        $query->andFilterWhere(['like', 'parent.title', $this->parent]);

        return $dataProvider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }
    
    public static function getTitleById($id = false) {
        $category = Category::findOne(["id" => $id]);
        return ($category)?$category->getAttribute('title'):"";
    }
    
    public static function getPathById($id = false) {
        $category = Category::findOne(["id" => $id]);
        return ($category)?$category->getAttribute('path'):"";
    }

}
