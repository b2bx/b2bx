<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $email
 * @property string $firstname
 * @property string $lastname
 * @property integer $birthday
 * @property integer $avatar
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord {
    
    public $_image;
    public $_temp_date;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['user_id', 'birthday'], 'integer'],
            ['email', 'email'],
            ['birthday', 'default', 'value' => null],
            [['firstname', 'lastname'], 'string', 'max' => 255],
            [['_temp_date'], 'date', 'format' => 'yyyy-MM-dd'],
            ['_image', 'file', 'extensions' => 'jpeg, jpg, gif, png', 'skipOnEmpty' => true],  
            ['avatar', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'email' => 'Email',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'birthday' => 'Birthday',
            '_temp_date' => 'Birthday',
            'avatar' => 'Avatar',
            '_image' => 'Select avatar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public static function loadModel($id = false) {
        $model = Profile::findOne(["user_id" => $id]);
        if (!$model) {
            $model = new Profile;
            $model->user_id = $id;
            $user = User::findOne(["id" => $id]);
            $model->email = $user->email;
        }
        if (!empty($model->birthday))
            $model->_temp_date = date("Y-m-d", $model->birthday);
        return $model;
    }
    
    public function uploadAvatar($images = false, $ext = "jpg") {
        if ($images) {
            foreach ($images as $image) {
                $temp = Yii::$app->image->set(["alias" => "avatar", "dir" => $this->user_id, "ext" => $ext]);
                try {
                    $thumb = $temp->open(isset($image->tempName)?$image->tempName:false);
                    if ($thumb->save(256, 256, true)) {
                        $this->avatar = $thumb->filename;
                    }
                }
                catch (Exception $e) {echo $e->getMessage(); die();}
            }
        }
    }
    
    public function getAvatar($option = ["small" => true]) {
        $photo = Yii::$app->image->set(["alias" => "avatar", "dir" => $this->user_id, "raw_data" => true, "default" => "default_avatar.jpg"]);
        $path = Yii::$app->image->getPathToImage();
        $photo->open($path, $this->avatar);
        if (isset($option['width']) && isset($option['height']))
            $photo->resize($option['width'], $option['height']);
        return $photo->getImage();
    }
}
