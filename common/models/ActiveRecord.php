<?php
namespace common\models;


abstract class ActiveRecord extends \yii\db\ActiveRecord {
    
    abstract public static function generateBaseModel();
    
    public static function getShortName() {
        return  (new \ReflectionClass(self::className()))->getShortName(); 
    }
    
    public static function getModelIsValid($id = false) {
        return ($model = static::findOne(["id" => $id]))?$model:false;
    }
    
    public function getRangeList($field = false) {
        $list = array();
        foreach ($this->getValidators() as $validator) {
            if ($validator instanceof \yii\validators\RangeValidator && $validator->range !== null && in_array($field, $validator->attributes)) {
                foreach($validator->range as $key => $value) {
                    $method = "get".ucfirst($field)."Attribute";
                    $list[$value] = (method_exists($this, $method))?$this->$method($value):$value;
                }
                break;
            }
        }
        return $list;
    }
}

