<?php

namespace common\models;

use Yii;
use common\models\Product;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "wish_list".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $user_id
 * @property integer $timestamp
 * @property integer $status
 *
 * @property User $user
 * @property Product $product
 */
class WishList extends ActiveRecord {
    
    const STATUS_NEW = 1;
    const STATUS_DELETED = 2;
    
    public $product_title;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'wish_list';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_id', 'user_id', 'timestamp', 'status'], 'integer'],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_DELETED]],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['product_title', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'user_id' => 'User ID',
            'timestamp' => 'Timestamp',
            'status' => 'Status',
            'product_title' => 'Product',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    public static function generateBaseModel() {
        $model = new WishList;
        $model->user_id = Yii::$app->user->id;
        $model->timestamp = time();
        return $model;
    }
    
    public static function isWish($number) {
        $product = Product::findProduct($number);
        $id = (!Yii::$app->user->isGuest)?Yii::$app->user->id:false;
        return $model = WishList::find()->where(["product_id" => $product->id, "user_id" => $id])->one();
    }
    
    public function search($params, $user_id = false, $only_new = true) {
        $query = WishList::find();
        $query->select('{{%product}}.`title`, {{%wish_list}}.*');
        $query->joinWith(["product"]);
        if ($user_id)
            $query->andWhere(["user_id" => $user_id]);
        if ($only_new)
            $query->andWhere(["status" => self::STATUS_NEW]);
        $query->groupBy("{{%product}}.`id`");
        $query->orderBy("`timestamp` DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'pagination' => ['pageSize' => 20],
        ]);

        // enable sorting for the related column
        $dataProvider->sort->attributes['product_title'] = [
            'asc' => ['product.title' => SORT_ASC],
            'desc' => ['product.title' => SORT_DESC],
        ];
        
        // load the seach form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        return $dataProvider;
    }
    
    public static function clearItems($period_in_days = 30) {
        $now = time();
        $days = $period_in_days * 24 * 3600; 
        $befor_time = $now - $days;
        return WishList::updateAll(['status' => self::STATUS_DELETED], "timestamp <= :timestamp", [":timestamp" => $befor_time]);
    }

}
