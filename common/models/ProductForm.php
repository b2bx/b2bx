<?php

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\Product;
use common\models\ProductDetail;
use common\models\ProductImage;
use common\models\Category;

/**
 * UploadForm is the model behind the upload form.
 */
class ProductForm extends  Product{

    public $description;
    public $category;
    public $image;
    public $warehouse_code;
    public $imageList = [];

    public function rules() {
        return array_merge(parent::rules(), [
            [['description'], 'string', 'max' => 255],
            ['image', 'file', 'extensions' => 'jpeg, jpg, gif, png', 'maxFiles' => 5],  
            ['warehouse_code', 'string'],
        ]);
    }
    
    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'description' => 'Description',
            'category' => 'Category'
        ]);
    }

    
    

    public function saveModel() {
        $detail = new ProductDetail;
        $detail->product_id = 1;
        $detail->description = $this->description;
        if ($this->validate() && $detail->validate()) {
            $this->save();
            $detail->product_id = $this->id;
            $detail->save();
            //Сохраняем набор изображений, если выбраны
            $this->uploadImages($this->image);
            return $this->id;
        }
        return false;
    }

    public function updateModel($id = false) {
        $detail = ProductDetail::findOne(["product_id" => $id]);
        $detail->product_id = $id;
        $detail->description = $this->description;
        $this->warehouse = $this->warehouse_code;
        if ($this->validate() && $detail->validate()) {
            $this->update();
            $detail->update();
            //Сохраняем набор изображений, если выбраны
            $this->uploadImages($this->image);
            return $id;
        }
        return false;
    }

    public function loadModel($id = false) {
        $this->store_id = \common\models\Store::homeStore()->id;
        $this->quantity = 1;
        $this->price = "1.0";
        $this->visible = 0;
        $this->generateNumber();
        if ($id) {
            $product = Product::findOne(["id" => $id]);
            if ($product) {
                $this->id = $id;
                $this->isNewRecord = false;
                $this->store_id = $product->store_id;
                $this->category_id = $product->category_id;
                $this->category = Category::getTitleById($product->category_id);
                $this->number = $product->number;
                $this->title = $product->title;
                $this->quantity = $product->quantity;
                $this->price = $product->price;
                $this->visible = $product->visible;

                $this->warehouse = $product->warehouse;
                $this->pieces = $product->pieces;
                $this->pieces_per_unit = $product->pieces_per_unit;
                $this->unit = $product->unit;
                $this->price_per_unit = $product->price_per_unit;
                $this->currency = $product->currency;
                $this->moq = $product->moq;
                
                $detail = ProductDetail::findOne(["product_id" => $id]);
                $this->description = $detail->description;
                $images = ProductImage::findAll(["product_id" => $id]);
                foreach ($images as $image) {
                    $this->imageList[$image->id] = ["small" => $image->getImageRawDada(["width" => 200, "height" => 150]), "main" => $image->filename];
                }
            }
        }
        return $this;
    }

}
