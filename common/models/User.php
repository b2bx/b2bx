<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
  * @property integer $deny_to
  * @property string $note
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 0;
    const STATUS_NEW = 1;
    const STATUS_DENY = 2;
    const STATUS_HIDDEN = 3;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['username', 'unique'],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_NEW, self::STATUS_DENY, self::STATUS_HIDDEN]],
            ['note', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => [self::STATUS_ACTIVE, self::STATUS_HIDDEN]]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => [self::STATUS_ACTIVE, self::STATUS_HIDDEN]]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => [self::STATUS_ACTIVE, self::STATUS_HIDDEN],
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile() {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }
    
    public static function setStatusActive($id) {
        return static::updateAll(["status" => self::STATUS_ACTIVE], ["id" => $id]);
    }
    
    public static function setStatusNewUser($id) {
        return static::updateAll(["status" => self::STATUS_NEW], ["id" => $id]);
    }
    
    public static function setStatusDeny($id, $to = false) {
        return static::updateAll(["status" => self::STATUS_DENY], ["id" => $id]);
    }
    
    public static function setStatusHidden($id) {
        return static::updateAll(["status" => self::STATUS_HIDDEN], ["id" => $id]);
    }
    
    public function getStatusAttribute($status_state = false) {
        $status = "none";
        $s = ($status_state)?$status_state:$this->status;
        switch($s) {
            case User::STATUS_DELETED: 
                $status = Yii::t("client", "Deleted");
                break;
            case User::STATUS_NEW: 
                $status = Yii::t("client", "New");
                break;
            case User::STATUS_DENY: 
                $status = Yii::t("client", "Deny");
                break;
            case User::STATUS_ACTIVE: 
                $status = Yii::t("client", "Active");
                break;
            case User::STATUS_HIDDEN: 
                $status = Yii::t("client", "Hidden");
                break;
        }
        return $status;
    }
    
    public function getStatusList() {
        $list = array();
        foreach ($this->getValidators('status') as $validator) {
            if ($validator instanceof \yii\validators\RangeValidator && $validator->range !== null) {
                foreach($validator->range as $key => $value) 
                    $list[$value] = $this->getStatusAttribute($value);
                break;
            }
        }
        return $list;
    }
    
    public static function getUsername($id = false) {
        $user = User::find()->where(["id" => $id])->one();
        return ($user)?$user->username:false;
    }
    
    public static function getUserProfile($id = false) {
        return $user = User::find()->with('profile')->where(["id" => $id])->one();
    }
    
    public function isHidden() {
        return ($this->status == self::STATUS_HIDDEN)?true:false;
    }

}
