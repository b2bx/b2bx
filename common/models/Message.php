<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
   @property string $model
 * @property integer $model_id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $text
 * @property integer $timestamp
 * @property integer $status
 *
 * @property Product $product
 * @property User $user
 * @property View[] $views
 */
class Message extends ActiveRecord {
    
    public $_is_out = false;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['model', 'from_user_id'], 'required'],
            [['timestamp', 'status'], 'integer'],
            [['model_id', 'from_user_id', 'to_user_id'], 'integer'],
            [['text'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'from_user_id' => 'User ID from',
            'to_user_id' => 'User ID to',
            'text' => 'Text',
            'timestamp' => 'Timestamp',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom() {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo() {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getView() {
        return $this->hasOne(View::className(), ['message_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViews() {
        return $this->hasMany(View::className(), ['message_id' => 'id']);
    }
    
    private function buildParams($params) {
        $default = [
            "from" => (!Yii::$app->user->isGuest)?Yii::$app->user->id:false,
            "to" => null,
            "model" => $this::getShortName(),
            "model_id" => null,
            "text" => ""
        ];
        foreach ($params as $key => $value) {
            if ($key == 'model' && is_object($value)) {
                $default[$key] = (new \ReflectionClass($value))->getShortName();
                if (!empty($value->id)) 
                    $default['model_id'] = $value->id;
            }
            else $default[$key] = $value;
        }
        return $default;
    }
    
    public static function generateBaseModel($params = []) {
        $model = new Message;
        $params = $model->buildParams($params);
        //$model->product_id = $product_id;
        $model->model = $params['model'];
        $model->model_id = $params['model_id'];
        $model->from_user_id = $params['from'];
        $model->to_user_id = $params['to'];
        $model->text = $params['text'];
        $model->timestamp = time();
        $model->status = 1;
        return $model;
    }
    
    public function countMessages($user_id = false) {
        return Message::find()->where(["to_user_id" => $user_id])->count();
    }
    
    public function countNewMessages($user_id = false) {
        $count = $this->countMessages();
        $_count = View::find()->where(["to_user_id" => $user_id])->count();
        $result = $count - $_count;
        return ($result > 0)?$result:0;
    }
    
    public static function countComments($model) {
        return Message::find()->where(["model" => $model->getShortName(), "model_id" => $model->id])->count();
    }
    
    public static function checkNewMessages($user_id = false, $product_id = false) {
        $query = Message::find();
        $query->joinWith("view");
        $query->where("`view`.id IS NULL");
        if ($user_id)
            $query->andWhere(["to_user_id" => $user_id]);
        if ($product_id)
            $query->andWhere(["product_id" => $product_id]);
        $messages = $query->all();
        foreach ($messages as $message) {
            $view = View::generateBaseModel(($user_id)?$user_id:Yii::$app->user->id, $message->id);
            $view->save();
        }
    }
    
    public static function Send($modelName = false, $params = []) {
        $params['model'] = ($modelName)?$modelName:"Object";
        $model = Message::generateBaseModel($params);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save ();
                $model->text = "";
                return $model;
            }
        }
        return false;
    }
    
    public function searchByProduct($params, $id = false) {
        if (!$id) return new ActiveDataProvider ();
        return $this->search($params, false, false, false, Product::getShortName(), $id);
    }
    
    public function searchByTrade($params, $id = false) {
        if (!$id) return new ActiveDataProvider ();
        return $this->search($params, false, false, false, Trade::getShortName(), $id);
    }
    
    public function searchByInquery($params, $id = false) {
        if (!$id) return new ActiveDataProvider ();
        return $this->search($params, false, false, false, Inquery::getShortName(), $id);
    }
    
    public function search($params, $user_id = false, $_new = false, $out = false, $model = false, $model_id = false) {
        $this->_is_out = $out;
        $query = Message::find();
        $query->joinWith("view");
        if ($_new) 
            $query->andWhere("`view`.id IS NULL");
        if ($user_id) {
            if (!$out)
                $query->andWhere(["to_user_id" => $user_id]);
            else $query->andWhere(["from_user_id" => $user_id]);
        }
        if ($model && $model_id) 
            $query->andWhere(["model" => $model, "model_id" => $model_id]);
        
        $query->orderBy("`timestamp` DESC");
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 5],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'number', $this->number]);

        return $dataProvider;
    }
    
    public function formatDate($timestamp = false) {
        if (!$timestamp) $timestamp = $this->timestamp;
        if (!$timestamp) return "";
        $datetime1 = new \DateTime('@'.time() );
        $datetime2 = new \DateTime('@' . $timestamp );
        $interval = $datetime1->diff($datetime2);

        $years = $interval->format('%y');
        $months = $interval->format('%m');
        $days = $interval->format('%a');
        $hours = $interval->format('%h');
        $minutes = $interval->format('%i');
        $seconds = $interval->format('%S');

        $elapsed = "Now";
        if($seconds && $seconds != "00"):
            $elapsed = $seconds == 1 ? $seconds . ' Second ' : $seconds . ' Seconds ';
        endif;
        if($minutes):
            $elapsed = $minutes == 1 ? $minutes . ' Minute ' : $minutes . ' Minutes ';
        endif;
        if($hours):
            $elapsed = $hours == 1 ? $hours . ' Hour ' : $hours . ' Hours ';
        endif;
        if($days):
            $elapsed = $days == 1 ? $days . ' Day ' : $days . ' Days ';
        endif;
        if($months):
            $elapsed = $months == 1 ? $months . ' Month ' : $months . ' Months ';
        endif;
        if($years):
            $elapsed = $years == 1 ? $years . ' Year ' : $years . ' Years ';
        endif;

        return $elapsed;
    }
    
    public function autorInformation($out = false, $show_info = false) {
        if (!$show_info) return;
        if ($this->model == $this->getShortName()) { //Сообщение, пока не используется
            return;
        }
        else if ($this->model) {
            //$link = ($this->model_id);
            return $text = ($this->_is_out?"from":"to")." {$this->model}";
        }
    }

}
