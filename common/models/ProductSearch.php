<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ProductSearch extends Product {

    public $category;
    public $description;
    public $image;

    public function rules() {
        // only fields in rules() are searchable
        return [
            [['id'], 'integer'],
            [['category', 'description', 'image'], 'safe'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), 
            ['description' => 'Description'],
            ['image' => 'Image']
        );
    }

    public function search($params, $ids = false) {       
        $query = Product::find();
        $query->select('`detail`.`description`, `image`.*, `product`.*');
        $query->joinWith(['detail' => function($query) {
                $query->from(['detail' => 'product_detail']);
            },
            'image' => function($query) {
                $query->from(['image' => 'product_image']);
            },
            'category']
        );
        if ($ids)
            $query->where(["product.id" => $ids]);

        //print_r($query->createCommand()->getRawSql()); die();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        // enable sorting for the related column
        $dataProvider->sort->attributes['description'] = [
            'asc' => ['detail.description' => SORT_ASC],
            'desc' => ['detail.description' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['category'] = [
            'asc' => ['category.title' => SORT_ASC],
            'desc' => ['category.title' => SORT_DESC],
        ];

        // load the seach form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        // adjust the query by adding the filters
        $query->andFilterWhere(['product.id' => $this->id]);
        $query->andFilterWhere(['store_id' => $this->store_id])
                ->andFilterWhere(['category_id' => $this->category_id])
                ->andFilterWhere(['like', 'number', $this->number])
                ->andFilterWhere(['like', 'title', $this->title]);
                //->andFilterWhere(['quantity', $this->quantity])
                //->andFilterWhere(['price', $this->price]);
        $query->andFilterWhere(['like', 'detail.description', $this->description]);
        $query->andFilterWhere(['like', 'category.title', $this->category]);

        return $dataProvider;
    }

}
        