<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\ProductImage;

class ImagesController extends Controller {

    public function actionIndex($number, $filename) {
        return Yii::$app->image->set(["dir" => $number, "filename" => $filename])->getImage();
    }
    
    public function actionSmall($number, $filename) {
        return Yii::$app->image->set(["dir" => $number, "filename" => $filename])->getImage(true);
    }

}
