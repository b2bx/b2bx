<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\UserProfileForm;
use common\models\UserProfileSearch;
use common\models\Profile;
use common\models\User;
use yii\web\UploadedFile;

class ProfileController extends Controller {

    public function actionIndex() {
        $model = UserProfileSearch::getUser(Yii::$app->user->id);
        
        return $this->render('index', ['model' => $model]);
    }
    
    public function actionUpdate() {
        $model = Profile::loadModel(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstances($model, '_image');
            if ($model->_temp_date) 
                $model->birthday = strtotime($model->_temp_date);
            if ($model->validate()) {
                $model->uploadAvatar($image);
                if ($model->isNewRecord)
                    $model->save ();
                else $model->update();
                return $this->redirect("index");
            }
            //else {print_r($model->getErrors ()); die(); }
        }

        return $this->render('_form', ['model' => $model]);
    }

}
