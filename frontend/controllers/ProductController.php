<?php

namespace frontend\controllers;

use Yii;
use common\models\Product;
use common\models\ProductForm;
use \common\models\Message;
use common\models\Inquery;
use common\models\WishList;

class ProductController extends \yii\web\Controller {

    public function actionIndex() {
        return $this->render('index');
    }
    
    public function actionView($number) {    
        $product = Product::findProduct($number);
        if (!$product)
            $this->redirect("/site/index");
        $message = Message::generateBaseModel(['model' => $product]);
        $inquery = Inquery::generateBaseModel($number);
        
        //Обрабатываем запросы от пользователя (lighteBox)
        $this->sendRequest($product);
        
        $model = new ProductForm;
        $model->loadModel($product->id);
        $searchModel = new Message();
        $dataProvider = $searchModel->searchByProduct(Yii::$app->request->get(), $product->id);
                
        return $this->render('view',  ['model' => $model, "searchModel" => $searchModel, "dataProvider" => $dataProvider, "message" => $message, "inquery" => $inquery]);
    }
    
    private function sendRequest($model) {
        //Если есть сообщение, то обрабатываем его и завершаем запрос
        if (Message::Send($model::getShortName())) 
            Yii::$app->end();
        
        //Если есть тикет, то обрабатываем его и завершаем запрос
        list($controller) = Yii::$app->createController('inquery');
        $controller->actionForm($model->number, true);
    }
    
    public function actionWish($number) {
        $model = WishList::isWish($number, Yii::$app->user->id);
        if (!$model) {
            $model = WishList::generateBaseModel();
            if ($product = Product::findProduct($number))
                $model->product_id = $product->id;
        }
        $model->timestamp = time();
        $model->status = WishList::STATUS_NEW;
        if ($model->validate()) {
            $model->save();
            return true;
        }
        
    }
    
    public function actionWishlist() {
        $searchModel = new WishList();
        $dataProvider = $searchModel->search(Yii::$app->request->get(), Yii::$app->user->id);
        
        return $this->render('wishlist', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }
    
    public function actionDeletefromwishlist($id) {
        WishList::updateAll(["status" => WishList::STATUS_DELETED], ["id" => $id]);
    }

}
