<?php

namespace frontend\controllers;

use Yii;
use \common\models\Product;
use \common\models\Category;

class CategoryController extends \yii\web\Controller {

    public function actionIndex($path) {
        $category = Category::findOne(["path" => $path]);
        $id = ($category)?$category->id:false;
        $menu = Category::buildMenu($id);
        $searchModel = new Product;
        $dataProvider = $searchModel->search(Yii::$app->request->get(), $id);
        
        return $this->render('index', ["menu" => $menu, "category" => $category, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

}
