<?php

namespace frontend\controllers;

use Yii;
use common\models\Message;

class MessageController extends \yii\web\Controller {

    public function actionIndex($new = false, $out = false) {
        if (Yii::$app->user->isGuest)
            return $this->redirect ("/site/index");
        
        $model = $this->actionSend();
        
        $title = ($new)?"new":($out?"outbox":"incoming");
        $searchModel = new Message();
        $dataProvider = $searchModel->search(Yii::$app->request->get(), Yii::$app->user->id, $new, $out);
        
        //if ($new)  //Если есть не прочитанные сообщения, метим их как прочитанные
        //    Message::checkNewMessages(false, 71);
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, "title" => $title, "model" => $model, "is_out" => $out]);
    }
    
    public function actionNew() {
        $content = $this->actionIndex(true);
        Message::checkNewMessages(Yii::$app->user->id);
        return $content;
    }
    
    public function actionOut() {
        return $this->actionIndex(false, true);
    }
    
    public function actionSend($params = []) {
        if (Message::Send(Message::getShortName()))
            Yii::$app->end();
        return Message::generateBaseModel();
    }
    
}
