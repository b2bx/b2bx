<?php

namespace frontend\controllers;

use Yii;
use \yii\web\Controller;
use common\models\Inquery;
use common\models\Product;
use common\models\Message;

class InqueryController extends Controller {
    
    public function actionIndex() {
        if (Yii::$app->user->isGuest)
            return $this->redirect ("/site/index");
        $searchModel = new Inquery();
        $dataProvider = $searchModel->search(Yii::$app->request->get(), Yii::$app->user->id, false);
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionForm($number, $skip = false) {
        if (Yii::$app->user->isGuest)
            return $this->redirect ("/site/index");
        $model = Inquery::generateBaseModel($number);
        $model->_skip = $skip;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save (); //Так же добавляет новую запись в таблицу Message
                return $this->render('_form', ["model" => $model, "number" => $number]);
                Yii::$app->end();
            }
        }
        return $model;
    }
    
    public function actionView($id = false) {
        $model = Inquery::findOne(["id" => $id]);
        
        $message = Message::generateBaseModel(['model' => $model]);
        //Если есть сообщение, то обрабатываем его и завершаем запрос
        if (Message::Send()) 
            Yii::$app->end();
        
        $searchModel = new Message();
        $dataProvider = $searchModel->searchByInquery(Yii::$app->request->get(), $model->id);

        return $this->render('view', ['model' => $model, "searchModel" => $searchModel, "dataProvider" => $dataProvider, "message" => $message]);
    }
    
    public function actionReply($id = false) {
        $model = Inquery::prepareForReply($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($product = Product::findOne(["id" => $model->product_id]))
                $model->number = $product->number;
            if ($model->validate()) {
                $model->save();
                return $this->redirect("index");
            }
        }

        return $this->render('reply', ['model' => $model, "id" => $id, "reply" => true]);
    }
    
    public function actionClose($id) {
        Inquery::updateAll(["status" => Inquery::STATUS_CLOSED], ["id" => $id]);
    }

}
