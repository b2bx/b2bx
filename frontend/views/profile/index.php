<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->params['breadcrumbs'] = [ 
    "User profile"
];
?>
<h1><?= $model->username ?> profile</h1>

<?php
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        
        [
            'label' => "Avatar",
            'format' => ['image',['width'=>'64','height'=>'64']],
            'value' => !empty($model->profile)?$model->profile->getAvatar(["width" => 64, "height" => 64]):Yii::$app->image->getDefaultAvatar(64, 64),
        ],
        [
            'label' => 'e-Mail',
            'value' => !empty($model->profile->email)?$model->profile->email:$model->email,
        ],
        'profile.firstname',
        'profile.lastname',
        [
            'label' => 'Birthday',
            'value' => !empty($model->profile->birthday)?date("Y-m-d", $model->profile->birthday):false,
        ]
    ],
]);
?>

<a href="/profile/update" class="btn btn-success">Update</a>

