<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfileSearch */
/* @var $form ActiveForm */
$this->params['breadcrumbs'] = [ 
    ["label" => "User profile", "url" => "/profile/index"],
    "Editing"
];
?>
<div class="profile-index">

    <?php 
    echo Html::img($model->getAvatar(), ["width" => 128, "height" => 128, 'class' => 'user-avatar']);
    $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
    ]);
    ?>

        <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
        <?php
        echo $form->field($model, '_image', [
            'template' => 
            '<div class="input-group file-input">
                    <span class="input-group-btn">
                        <span class="btn btn-primary btn-file">Add avatar {input} </span>
                    </span>
                    <input type="text" class="form-control" readonly="">
                </div>',
        ])->fileInput(['multiple' => false]);
        ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'firstname') ?>
        <?= $form->field($model, 'lastname') ?>
        <?php 
        echo $form->field($model, '_temp_date')->widget(DatePicker::className(), [
            'dateFormat' => 'yyyy-MM-dd',
            'options' => [
                'autoclose' => true,
            ]
        ]);
        ?>
    
        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- profile-index -->
