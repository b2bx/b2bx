<?php
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\web\JsExpression;

Modal::begin([
    'id' => "{$id}",
    'header' => '<h3 id="message-header">Make a request</h3>',
    'clientEvents' => [
        'show.bs.modal' => new JsExpression("function(event){
            //var button = $(event.relatedTarget);
            //var id = button.data('userid');
            //var username = button.data('username');
            //var modal = $(this);
            //modal.find('#message-header').text('Send message to ' + username);
            //modal.find('#message-to_user_id').val(id);
        }"),
    ]
]);


$this->registerJs(
   '$("document").ready(function(){ 
        $("#_send_message2").on("pjax:end", function() {
            $("#messageForm").modal("hide");
        });
    });'
);
echo "<div class=\"form\">";
Pjax::begin(['id' => '_send_message2']);
echo $this->render("_form", ["model" => $model, "label" => true]);
Pjax::end();
echo "</div>";
Modal::end();
