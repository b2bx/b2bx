<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Product;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\Inquery */
/* @var $form ActiveForm */

$product = Product::findProduct($model->number);
$breadcrumbs = [ 
    ["label" => "Categories", "url" => "/category/index"],
];
if (!empty($product->category_id))
    $breadcrumbs[] = ["label" => Category::getTitleById($product->category_id), "url" => "/category/".Category::getPathById($product->category_id)];
if (!empty($product))
    $breadcrumbs[] = ["label" => $product->title, "url" => "/product/".$model->number];
$breadcrumbs[] = "Start new ticket";
$this->params['breadcrumbs'] = $breadcrumbs;
?>
<div class="form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'product_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'quantity')->textInput()->label("Interested order amount") ?>
        <?= $form->field($model, 'timestamp')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'price')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'number')->hiddenInput()->label(false); //->textInput(['readonly' => true]) ?>
        <?= $form->field($model, 'message')->textarea()->label("Ask your question") ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('inquery', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- _form -->
