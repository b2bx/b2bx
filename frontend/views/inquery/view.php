<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use common\models\Product;
use common\models\User;
use common\models\Inquery;
use common\models\Message;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Inquery */
/* @var $form ActiveForm */
$this->params['breadcrumbs'] = [ 
    ["label" => "Inqueries", "url" => "/inquery/index"],
    "View inquery", 
];
?>
<h1>View inquery</h1>
<?php 
$product = ($model->product_id)?Product::isValid($model->product_id):false;
Pjax::begin(['id' => '_inquery_list']);
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        
        [
            'label' => 'Product',
            'format'=>'raw',
            'value' => !empty($product)?Html::a($product->title, "/product/".$model->number):$model->number,
        ],
        [
            'label' => 'Date',
            'value' => $model->timestamp?date("Y-m-d G:i:s", $model->timestamp):"none",
        ],
        'quantity',
        'price',
        [
            'label' => 'Notice',
            'attribute'=>'message',
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => $model->getStatusAttribute(),
        ],
    ]
]);
Pjax::end();
?>

<a href="/inquery/reply?id=<?= $model->id ?>" class="btn btn-success">Reply</a>
<?php 
if ($model->status != Inquery::STATUS_CLOSED) { 
?>
    <a href="/inquery/close?id=<?= $model->id ?>" class="btn btn-info button-action" reload="_inquery_list">Close</a>
<?php 
} 
?>
<h1>Messages</h1>
<?= $this->render("/message/_list", ['dataProvider' => $dataProvider, "model" => Message::generateBaseModel(), "message" => $message, "can_send_message" => true]) ?>


