<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;
use common\models\Product;

$this->params['breadcrumbs'] = [ 
    "Inqueries"
];
?>
<h1>My inqueries overview</h1>

<p>
    <?php
    $gridColumns = [
//        'id' => [
//            'attribute' => 'id',
//            'filter' => false,
//        ],
        [
            'attribute' => 'number',
            'format' => 'raw',
            'filter' => false,
            'value' => function($model) {
                return ($product = Product::isValid($model->product_id))?Html::a($product->title, ["product/".$product->number]):$model->number;
            }
        ],
        [
            'attribute' => 'message',
            'filter' => false,
        ],
        'quantity' => [
            'attribute' => 'quantity',
            'filter' => false,
        ],
        'price' => [
            'attribute' => 'price',
            'filter' => false,
        ],
        'timestamp' => [
            'attribute' => 'timestamp',
            'format' => 'raw',
            'filter' => false,
            'value' => function ($model) {                      
                return ($model->timestamp)?date("Y-m-d G:i:s", $model->timestamp):"";
            },
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'filter' => false,
            'value' => function($model) {
                return $model->getStatusAttribute();
            },
        ],
        [  
        'class' => 'yii\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:110px;'],
        'header'=>'Actions',
        'buttons'=>[
            'view' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                    'title' => Yii::t('yii', 'View'),
                ]);
            },
            
            'reply' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-repeat"></span>', $url, [
                    'title' => Yii::t('yii', 'Reply'),
                ]);
            },
            'close' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, [
                    'title' => Yii::t('yii', 'Close'),
                    'data-pjax' => '0', 
                    'class' => 'grid-action'
                ]);
            },
        ],
        'template' => '{view} {reply} {close}',

       ],

    ];
    Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]);
    Pjax::end();
    ?>
</p>
<p>
    <a href="/wishlist" class="btn btn-success">Show wish list</a>
</p>
