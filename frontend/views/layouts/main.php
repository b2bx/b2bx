<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'B2B-X Project',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
                //['label' => 'About', 'url' => ['/site/about']],
                
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
               
                
                $menuItems[] = ['label' => 'Inqueries', 'url' => ['/inquery/index'], 'items' => [
                    ['label' => 'Inqueries overview', 'url' => ['/inquery/index']],
                    ['label' => 'Wish list', 'url' => ['/wishlist']],
                ]];
                $menuItems[] = ['label' => 'Messages', 'url' => ['/message/index'], 'items' => [
                    ['label' => 'Messages'],
                    ['label' => 'View new messages', 'url' => ['/message/new']],
                    ['label' => 'Incoming messages', 'url' => ['/message/index']],
                    ['label' => 'Outbox messages', 'url' => ['/message/out']],
                    ['label' => 'Help'],
                    ['label' => 'About', 'url' => ['/site/about']],
                    ['label' => 'Support', 'url' => ['/site/contact']],
                ]];
                $menuItems[] = ['label' => 'Profile', 'url' => ['/profile/index'], 'items' => [
                    ['label' => 'Contact detail', 'url' => ['/profile/index']],
                    ['label' => 'Billing information', 'url' => ['/profile/billing']],
                ]];
                //$menuItems[] = ['label' => 'Contact', 'url' => ['/site/contact']];
                
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
