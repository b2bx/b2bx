<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin([
    //"action" => ["message/send"],
    'options' => ['data-pjax' => 1 ]
]); ?>
    <?= $form->field($model, 'model')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'model_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'from_user_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'to_user_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'text')->textarea()->label((!empty($label)?$label:false)) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('inquery', 'Send'), ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>

