<?php
use yii\helpers\Html;

$user = common\models\User::getUserProfile($model->from_user_id);
?>
<div class="message row">
    <div class="user-box">
        <div class="avatar"><?= (!empty($user->profile))?Html::img($user->profile->getAvatar(["width" => 48, "height" => 48]), ["width" => 48, "height" => 48, 'class' => 'user-avatar']):Html::img(Yii::$app->image->getDefaultAvatar(48, 48), ['class' => 'user-avatar']); ?></div>
    </div>
    <div class="col-md-11 _body">
        <div class="head">
            <div class="autor"><?= (!$user->isHidden())?$user->username:"Is hidden" ?></div>
            <div class="date" title="<?= date("Y-m-d G:i:s", $model->timestamp) ?>"><?= $model->formatDate() ?></div>
            <div class="source"><?= $model->autorInformation($is_out, $show_info) ?></div>
        </div>
        <div class="col-md-11 text"><?= $model->text ?></div>
    </div>
    <?php if (!Yii::$app->user->isGuest) { ?>
    <ul class="buttons">
        <?php if ($user->id != Yii::$app->user->id) { ?>
        <a href="#" data-toggle="modal" data-target="#messageForm" data-userid="<?= $user->id ?>" data-username="<?= $user->username ?>"><li class="glyphicon glyphicon-send"></li></a>
        <?php } ?>
        <!--<a href="#"><li class="glyphicon glyphicon-paperclip"></li></a>-->
    </ul>
    <?php } ?>
</div>

