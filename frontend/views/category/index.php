<?php
use \nirvana\prettyphoto\PrettyPhoto;
use \yii\widgets\LinkPager;
use common\models\Inquery;
use yii\helpers\Html;

$this->params['breadcrumbs'] = [ 
    ["label" => "Categories", "url" => "/"],
    @$category->title
];

/* @var $this yii\web\View */
$this->title = 'B2B-X Application';
PrettyPhoto::widget([
    'target' => "a[rel^='prettyPhoto']",
    'pluginOptions' => [
        'opacity' => 0.60,
        'theme' => PrettyPhoto::THEME_FACEBOOK,
        'social_tools' => false,
        'autoplay_slideshow' => false,
        'modal' => true
    ],
]);
?>
<div class="row">
    <div class="col-lg-3">
        <h1><?= Yii::t("category", "Categories") ?></h1>
        <?php
        echo \yii\bootstrap\Nav::widget([
            'items' => $menu,
            'options' => ['class' => 'nav-stacked'], // set this to nav-tab to get tab-styled navigation
        ]);
        ?>
    </div>
    <div class="col-lg-9">
        
        <h1><?= @$category->title." ".Yii::t('product', "products overview") ?></h1>
            <?php
            if ($dataProvider->totalCount) {
                $i = 0; $c = 2;
                foreach ($dataProvider->getModels() as $product) {
                    if (!$i++) echo "<div class=\"row\">";
            ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <?php if (!empty($product->image)) { ?>
                                <?php if (Yii::$app->user->isGuest) { ?>
                                    <a href="<?= "/images/{$product->number}/{$product->image->filename}" ?>" rel="prettyPhoto">
                                        <img src="<?= ($product->image ? $product->image->getImageRawDada() : "") ?>">
                                    </a>
                                <?php } else { ?>
                                    <a href="<?= "/product/{$product->number}" ?>"><img src="<?= ($product->image ? $product->image->getImageRawDada() : "") ?>" ></a>
                                <?php } ?>
                            <?php } else {
                                echo Html::img(Yii::$app->image->getDefaultImage(300, 200));
                            }
                            ?>
                            <div class="caption">
                                <h3><?= $product->title ?></h3>
                                <p><?= ($product->detail ? $product->detail->description : "") ?></p>
                                <?php if (!Yii::$app->user->isGuest) { ?>
                                <p><a href="<?= "/product/".$product->number ?>" class="btn btn-default" role="button">View</a></p>
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
            <?php
                    if ($i > $c) {$i = 0; echo "</div>";}
                }
                if ($i) {echo "</div>";}
                echo "<div class=\"row\" style=\"padding-left: 15px;\">";
                echo LinkPager::widget([
                    'pagination'=>$dataProvider->pagination,
                ]);
                echo "</div>";
            }
            ?>
    </div>
</div>