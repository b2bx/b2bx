<?php 
use \nirvana\prettyphoto\PrettyPhoto;

if (!empty($images)) {
    PrettyPhoto::widget([
        'target' => "a[rel^='prettyPhoto']",
        'pluginOptions' => [
            'opacity' => 0.60,
            'theme' => PrettyPhoto::THEME_FACEBOOK,
            'social_tools' => false,
            'autoplay_slideshow' => true,
            'modal' => true
        ],
    ]);
?>
<script type="text/javascript">
    $(document).on('click', '.file-preview div.close', function() {
        var id = $(this).attr("rel");
        $.get("/product/imagedelete", {id:id}, function(data) {
           if (data.success) 
               $(".file-preview #preview-" + id).remove();
        }, "json");
    });
</script>
<div class="file-preview">
    <div class="file-preview-thumbnails">
        <?php foreach ($images as $id => $image) { ?>
        <div class="file-preview-frame file-preview-initial" id="preview-<?= $id ?>">
            <a href="<?= "/images/{$number}/{$image['main']}" ?>" rel="prettyPhoto"><img src="<?= $image['small'] ?>" class="file-preview-image"></a>
        </div>
        <?php } ?>
        
    </div>
    <div class="close">x</div>
    <div class="clearfix"></div>
</div>
<?php } ?>
