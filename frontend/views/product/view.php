<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use \yii\widgets\DetailView;
use \yii\widgets\ListView;
use yii\widgets\ActiveForm;
use \common\models\Inquery;
use yii\widgets\Pjax;
use common\models\Message;
use common\models\Category;
use common\models\WishList;

/* @var $this yii\web\View */
/* @var $model common\models\ProductForm */
/* @var $form ActiveForm */

$this->params['breadcrumbs'] = [ 
    ["label" => "Categories", "url" => "/category/index"],
];
if (!empty($model->category_id))
    $this->params['breadcrumbs'][] = ["label" => Category::getTitleById($model->category_id), "url" => "/category/".Category::getPathById($model->category_id)];
$this->params['breadcrumbs'][] = $model->title;
?>
<h1><?= Yii::t("product", "Detail view of <span>").$model->title."</span> product" ?></h1>

<?php 
    echo $this->render("_images", ["images" => $model->imageList, "number" => $model->number]); 
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number',
            'title',
            'category',
            'description',
        ],
    ]);
?>
<?php if (!Yii::$app->user->isGuest) { ?>
<a href="/inquery/<?= $model->number ?>" class="btn btn-success" data-toggle="modal" data-target="#inqueryForm">Get more information</a>
<?php 
$wish = WishList::isWish($model->number);
if (!($wish &&  $wish->status == WishList::STATUS_NEW)) { ?>
<a href="/wish/<?= $model->number ?>" class="btn btn-primary button-action">Add to wish list</a>
<?php } ?>
<?php 
echo $this->render("/inquery/_request", ["model" => $inquery, "id" => "inqueryForm"]);
?>
<?php } ?>
<h1>Messages</h1>
<?= $this->render("/message/_list", ['dataProvider' => $dataProvider, "model" => Message::generateBaseModel(), "message" => $message]) ?>



