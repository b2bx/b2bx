<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use \yii\widgets\Pjax;
use common\models\Product;

$this->params['breadcrumbs'] = [ 
    ["label" => "Inqueries", "url" => "/inquery/index"],
    "Products wish list"
];
?>
<h1>Products wish list</h1>

<p>
    <?php
    $gridColumns = [
        [
            'attribute' => 'product_title',
            'format' => 'raw',
            'filter' => false,
            'value' => function($model) {
                return ($product = Product::isValid($model->product_id))?Html::a($product->title, ["product/".$product->number]):$model->product->title;
            }

        ],
        'timestamp' => [
            'label' => 'Date',
            'attribute' => 'timestamp',
            'format' => 'raw',
            'filter' => false,
            'value' => function ($model) {                      
                return ($model->timestamp)?date("Y-m-d G:i:s", $model->timestamp):"";
            },
        ],
        [  
        'class' => 'yii\grid\ActionColumn',
        'contentOptions' => ['style' => 'width:90px;'],
        'header'=>'Actions',
        'buttons'=>[
            'delete' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ["/wishlist/delete", "id" => $model->id], [
                    'title' => Yii::t('yii', 'Delete'),
                    'data-pjax' => '0', 
                    'class' => 'grid-action'
                ]);
            },
        ],
        'template' => '{delete}',

       ],
    ];
    Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]);
    Pjax::end();
    ?>
</p>
