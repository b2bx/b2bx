<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language'=>'en',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // your rules go here
                '/'=>'site/index',
                'category/<path:\w+>' => 'category/index',
                'images/<number:\w+-?\w+>/<filename:\w+\.\w+>' => '/images/index',
                'images/small/<number:\w+-?\w+>/<filename:\w+\.\w+>' => '/images/small',
                'inquery/index' => 'inquery/index',
                //'inquery/<number:\w+-?\w+>' => 'inquery/form',
                'wish/<number:\w+-?\w+>' => 'product/wish',
                'wishlist' => 'product/wishlist',
                'wishlist/delete' => 'product/deletefromwishlist',
                'message/list' => 'message/index',
                'product/<number:\w+-?\w+>' => 'product/view',
                //'profile/<username:\w+>' => 'profile/update',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@backend/messages',
                ],
                
            ]
        ],
        'image' => [
            'class' => 'common\components\Image',
            'ext' => 'jpg'
        ]
    ],
    'params' => $params,
];
