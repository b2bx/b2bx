/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        console.log(numFiles);
        console.log(label);
        
        if (numFiles > 1) label = numFiles + " file(s) selected";
        $(".file-input .form-control").val(label);
    });
});

$("document").ready(function(){ 
    $(".combobox" ).click(function() {
       $(this).autocomplete( "search", "" );
    });
});


$(function(){
    $('body').on('click', '.grid-action', function(e){
        var href = $(this).attr('href');
        var self = this;
        $.get(href, function(){
            var pjax_id = $(self).closest('.pjax-wraper').attr('id');
            $.pjax.reload('#' + pjax_id);
        });
        return false;
    });
    
    $('body').on('click', '.button-action', function(e){
        var el = $(this);
        var href = $(this).attr('href');
        var self = this;
        var reload = $(this).attr('reload');
        $.get(href, function(data){
            $(el).fadeOut();
            if (reload) {
                $.pjax.reload({container:"#"+reload});
            }
        });
        return false;
    });
});




